#  Simple top-level Makefile to point users to those hidden below:

CONFIG_FILE=config.h
include $(CONFIG_FILE)

install:
	cd $(AMBER_SOURCE)/AmberTools/src && $(MAKE) install
	@(if [ -n "$(BUILDAMBER)" ] ; then \
	    cd src && $(MAKE) install; \
	  else \
		echo "==============================================================" ;\
		echo "$(AMBER_SOURCE)/src/Makefile not found, or -noamber was set." ;\
		echo "This is expected if you do not have Amber18." ;\
		echo "==============================================================" ;\
	fi ;\
	)

sander:
	cd $(AMBER_SOURCE)/AmberTools/src/sander && $(MAKE) install

pmemd:
	cd $(AMBER_SOURCE)/src && $(MAKE) pmemd_only

python:
	cd $(AMBER_SOURCE)/AmberTools/src && $(MAKE) packmol-memgen parmed mmpbsa_py pymsmt $(PYTRAJ)
	cd $(AMBER_SOURCE)/AmberTools/src/etc && $(MAKE) python
	# If it needs to be done, fix conda's libgfortran issue... sigh.
	$(PYTHON) $(AMBER_SOURCE)/AmberTools/src/fix_conda_fortran.py

clean:
	/bin/rm -rf updateutils/__pycache__ ./updateutils/*.pyc
	-(cd $(AMBER_SOURCE)/AmberTools/src && $(MAKE) clean)
	-(cd $(AMBER_SOURCE)/src && $(MAKE) clean)

uninstall:
	-(cd src && $(MAKE) uninstall)
	-(cd AmberTools/src && $(MAKE) uninstall)
	/bin/rm -rf bin/to_be_dispatched

clean.test: is_amberhome_defined
	-(cd AmberTools/test && $(MAKE) clean)
	-(cd test && $(MAKE) clean)

test.clean:  clean.test

# This target cleans all generated files excluding executables;
# the intention is that veryclean returns the distribution to a
# sufficient state for re-configuring with a different compiler.
veryclean: clean clean.test
	/bin/rm -rf miniconda include/* lib/*
	/bin/rm -f ./$(CONFIG_FILE) ./AmberTools/src/$(CONFIG_FILE)

# This target cleans all generated files including all executables;
# the intention is that distclean returns the distribution to a
# pristine state as though the user just untarred a fresh copy of the code and
# applied their updates.
# This target should not use target veryclean due to CONFIG_FILE removal
# inside the uninstall target.
distclean: clean clean.test uninstall
	/bin/rm -rf miniconda include/* lib/*
	/bin/rm -rf updateutils/__pycache__ ./updateutils/*.pyc
	/bin/rm -rf AmberTools/src/pytraj/scripts/*.pyc
	/bin/rm -f ./$(CONFIG_FILE) ./AmberTools/src/$(CONFIG_FILE)

test::  test.$(INSTALLTYPE)

test.serial: is_amberhome_defined
	-@(if [ "$(SKIP_PYTHON)" = "no" ] ; then \
		cd AmberTools/test && $(MAKE) test; \
	else \
		cd AmberTools/test && $(MAKE) test.nopython; \
	fi ;\
	)
	@(if [ -n "$(BUILDAMBER)" ] ; then \
		cd test && $(MAKE) test; \
		echo ""; \
		echo "Summary of AmberTools serial tests:"; \
		echo ""; cat ../logs/test_at_serial/at_summary; \
	else \
		echo "==============================================================" ;\
		echo "$(AMBERHOME)/src/Makefile not found." ;\
		echo "This is expected if you have not installed Amber18." ;\
		echo "==============================================================" ;\
	fi ;\
	)

test.openmp: is_amberhome_defined
	(cd AmberTools/test && $(MAKE) test.openmp)

test.parallel: is_amberhome_defined
	-(cd AmberTools/test && $(MAKE) test.parallel)
	@(if [ -n "$(BUILDAMBER)" ] ; then \
		cd test && $(MAKE) test.parallel; \
		echo ""; \
		echo "Summary of AmberTools parallel tests:"; \
		echo ""; cat ../logs/test_at_parallel/at_summary; \
	else \
		echo "==============================================================" ;\
		echo "$(AMBERHOME)/src/Makefile not found." ;\
		echo "This is expected if you have not installed Amber18." ;\
		echo "==============================================================" ;\
	fi ;\
	)

test.cuda_serial: is_amberhome_defined
	-(cd test && $(MAKE) test.cuda.serial)

test.cuda_parallel: is_amberhome_defined
	-(cd test && $(MAKE) test.cuda.parallel)

test.mic_offload: is_amberhome_defined
	-(cd test && $(MAKE) test.mic_offload)

test.mic2: is_amberhome_defined
	-(cd test && $(MAKE) test.parallel.pmemd.mic2)

nabonly:
	-(cd AmberTools/src && $(MAKE) nabonly)

openmp:
	-(cd AmberTools/src && $(MAKE) openmp)

help:
	@echo "configure is the first step in the installation.  Execute this: "
	@echo "./configure --help"

is_amberhome_defined:
	@(if [ -z "$(AMBERHOME)" ] ; then \
	    echo "Error: AMBERHOME is not defined !" ;\
	    exit 2 ;\
	fi ;\
	)

