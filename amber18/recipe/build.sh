#!/bin/sh

export AMBERHOME=`pwd`

#  do following before starting the conda build; fails at pip
#    install if done during the conda build(?)
#  ./AmberTools/src/configure_python --prefix $AMBERHOME --version 2

if [ `uname` == "Darwin" ]; then
    ./configure -macAccelerate clang
    make install 
    make clean

    mkdir -p lib/amber_3rd_party # to avoid overwritten by other programs
    python ./recipe/copy_and_fix_gfortran.py \
        /usr/local/gfortran/lib lib/amber_3rd_party
    mkdir -p $PREFIX/lib
    rsync -av lib/amber_3rd_party $PREFIX/lib
else
    ./configure -static gnu
    make install 
    make clean
fi

rsync -a amber.sh amber.csh bin dat doc lib miniconda update_amber updateutils $PREFIX
