# Source this script to add the variables necessary to use Amber to your shell.
# This script must be located in the Amber root folder!

# Amber was configured on @CONFIGURATION_TIMESTAMP@

# get path to current script(credit http://serverfault.com/questions/139285/tcsh-path-of-sourced-file
set DUS = ( $_ ) #DUS: Dollar UnderScore
set DNU = $0:q   #DNU: Dollar NUll
if (( $#DUS > 1 )) then
	if ("${DUS[1]}" == 'source' || "$DNU:t" == 'tcsh' || "$DNU:t" == 'csh') then
		set DNU = ${DUS[2]:q}
	endif
endif

setenv AMBERHOME (cd "$DNU:h" >&! /dev/null; pwd)
setenv PATH "$AMBERHOME/bin:@EXTRA_PATH_PART@$PATH"

# Add Amber lib folder to LD_LIBRARY_PATH (if your platform supports it)
# Note that LD_LIBRARY_PATH is only necessary to help Amber's Python programs find their dynamic libraries, 
# unless Amber has been moved from where it was installed.
if(@AMBERSH_SUPPORTS_LDLIBPATH@ == 1) then
	if( ! ($?@LIB_PATH_VAR@) ) then
		setenv @LIB_PATH_VAR@ "@LIB_PATH_DIRECTORIES@:$@LIB_PATH_VAR@"
	else
		setenv @LIB_PATH_VAR@ "@LIB_PATH_DIRECTORIES@"
	endif
endif

# Add location of Amber Perl modules to default Perl search path (if your platform supports it)
if(@AMBERSH_PERL_SUPPORT@ == 1) then
	if( ! ($?PERL5LIB) ) then
		setenv PERL5LIB "$AMBERHOME/@AMBERSH_PERL_MODULE_DIR@"
	else
		setenv PERL5LIB "$AMBERHOME/@AMBERSH_PERL_MODULE_DIR@:$PERL5LIB"
	endif
endif

# Add location of Amber Python modules to default Python search path (if your platform supports it)
if( ! (\$?PYTHONPATH) ) then
	setenv PYTHONPATH "$AMBERHOME@AMBERSH_RELATIVE_PYTHONPATH@"
else
	setenv PYTHONPATH "$AMBERHOME@AMBERSH_RELATIVE_PYTHONPATH@:$PYTHONPATH"
endif