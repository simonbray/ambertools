
#FEW dependency perl modules to install
set(PERL_MODULES AtomWithType.pm Descriptive.pm Distributions.pm FreezeThaw.pm 
    Normality.pm PointEstimation.pm ReadBackwards.pm Smoother.pm)

#------------------------------------------------------------------------------------------

install(FILES ${PERL_MODULES} DESTINATION ${PERL_MODULE_INSTALL_DIR}/FEW_libs)
