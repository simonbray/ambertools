import os
import sys
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

mdgxhome = os.environ.get("MDGXHOME")
amberhome = os.environ.get("AMBERHOME")

if mdgxhome is not None:
    library_dirs = [os.path.join(mdgxhome, 'lib'),]
    include_dirs = [os.path.join(mdgxhome, 'include'),]
elif amberhome is not None:
    library_dirs = [os.path.join(amberhome, 'lib'),]
    include_dirs = [os.path.join(amberhome, 'include'),]
else:
	# called by CMake build script, use command-line args
	include_dirs = []
	library_dirs = []
	
	args_to_pass_on = []
	
	for arg in sys.argv:
		if arg.startswith('-I'):
			include_dirs.append(arg.replace('-I',''))
		elif arg.startswith('-L'):
			library_dirs.append(arg.replace('-L',''))
		else:
			args_to_pass_on.append(arg)
			
	sys.argv = args_to_pass_on


extra_compile_args = ['-O0', '-ggdb']
extra_link_args = ['-O0', '-ggdb']

cython_directives = {
    'embedsignature': True,
    'boundscheck': False,
    'wraparound': False,
}

ext = Extension(
        "mdgx.mdgx",
        ["mdgx/mdgx.pyx",],
        libraries=['mdgx', 'netcdf',],
        library_dirs=library_dirs,
        include_dirs=include_dirs,
        extra_compile_args=extra_compile_args,
        extra_link_args=extra_link_args,
        )

setup(
    name='mdgx',
    packages=['mdgx'],
    version='0.0.1',
    ext_modules=cythonize([ext, ],
        compiler_directives=cython_directives,
    ),
)
