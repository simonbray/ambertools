
__device__ __forceinline__ QUICKDouble quick_pow(QUICKDouble a, int power)
{
    /* 
        notice 0^0 = 1 for this subroutine but is invalid mathmatically
     */
    if (power == 0) {
        return 1;
    }
    if (power == 1) {
        return a;
    }
    if (power == 2) {
        return a*a;
    }
    
    if (power ==3) {
        return a*a*a;
    }
    return 0;
}

