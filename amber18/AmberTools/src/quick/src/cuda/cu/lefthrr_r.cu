
__device__ __forceinline__ int lefthrr_r(QUICKDouble RAx, QUICKDouble RAy, QUICKDouble RAz,
                                       QUICKDouble RBx, QUICKDouble RBy, QUICKDouble RBz,
                                       int KLMNAx, int KLMNAy, int KLMNAz,
                                       int KLMNBx, int KLMNBy, int KLMNBz,
                                       int IJTYPE,QUICKDouble* coefAngularL, int* angularL)
{
    
    coefAngularL[0] = 1.0;
    angularL[0] = (int) LOC3(devTrans, KLMNAx + KLMNBx, KLMNAy + KLMNBy, KLMNAz + KLMNBz, TRANSDIM, TRANSDIM, TRANSDIM);
    
    if (IJTYPE == 0) {
        return 1;
    }
    
    QUICKDouble tmpx = RAx - RBx;
    QUICKDouble tmpy = RAy - RBy;
    QUICKDouble tmpz = RAz - RBz;
    
    QUICKDouble tmpx2 = 1.0;
    QUICKDouble tmpy2 = 1.0;
    QUICKDouble tmpz2 = 1.0;
    
    if (KLMNBx > 0) {
        tmpx2 = quick_pow(tmpx, KLMNBx);
    }
    
    if (KLMNBy > 0) {
        tmpy2 = quick_pow(tmpy, KLMNBy);
    }
    
    if (KLMNBz > 0) {
        tmpz2 = quick_pow(tmpz, KLMNBz);
    }
    
    angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
    coefAngularL[1] = tmpx2 * tmpy2 * tmpz2;
    
    if (IJTYPE == 1) {
        return 2;
    }
    
    int numAngularL = 2;
    
    if (KLMNBx >= 1) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = KLMNBx * quick_pow(tmpx, KLMNBx-1) * tmpy2 * tmpz2;
        numAngularL++;
    }
    
    if (KLMNBy >= 1) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = KLMNBy * quick_pow(tmpy, KLMNBy-1) * tmpx2 * tmpz2;
        numAngularL++;
    }
    if (KLMNBz >= 1) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = KLMNBz * quick_pow(tmpz, KLMNBz-1) * tmpx2 * tmpy2;
        numAngularL++;
    }
    
    
    if (IJTYPE == 2) {
        return numAngularL;
    }
    
    if (KLMNBx >= 2) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBx * (KLMNBx - 1) / 2 * quick_pow(tmpx, KLMNBx-2) * tmpy2 * tmpz2;
        numAngularL++;
    }
    
    
    if (KLMNBy >= 2) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBy * (KLMNBy - 1) / 2 * quick_pow(tmpy, KLMNBy-2) * tmpx2 * tmpz2;
        numAngularL++;
    }
    
    if (KLMNBz >= 2) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBz * (KLMNBz - 1) / 2 * quick_pow(tmpz, KLMNBz-2) * tmpx2 * tmpy2;
        numAngularL++;
    }
    
    if (KLMNBx >= 1 && KLMNBy >= 1) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBx * KLMNBy * quick_pow(tmpx, KLMNBx-1) * quick_pow(tmpy, KLMNBy-1) * tmpz2;
        numAngularL++;
    }
    
    if (KLMNBx >= 1 && KLMNBz >= 1) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBx * KLMNBz * quick_pow(tmpx, KLMNBx-1) * quick_pow(tmpz, KLMNBz-1) * tmpy2;
        numAngularL++;
    }
    
    
    if (KLMNBy >= 1 && KLMNBz >= 1) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBy * KLMNBz * quick_pow(tmpy, KLMNBy-1) * quick_pow(tmpz, KLMNBz-1) * tmpx2;
        numAngularL++;
    }
    
    /* the last case is IJTYPE = 3 */
    return numAngularL;
    
}

