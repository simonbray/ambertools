
__device__ void vertical2_spdf2(int I, int J, int K, int L,
                                QUICKDouble* YVerticalTemp, QUICKDouble* store,
                                QUICKDouble Ptempx, QUICKDouble Ptempy, QUICKDouble Ptempz,  \
                                QUICKDouble WPtempx,QUICKDouble WPtempy,QUICKDouble WPtempz, \
                                QUICKDouble Qtempx, QUICKDouble Qtempy, QUICKDouble Qtempz,  \
                                QUICKDouble WQtempx,QUICKDouble WQtempy,QUICKDouble WQtempz, \
                                QUICKDouble ABCDtemp,QUICKDouble ABtemp, \
                                QUICKDouble CDtemp, QUICKDouble ABcom, QUICKDouble CDcom)
{
    if ((I+J) >=  5 && (K+L) >= 0) {
        h2_5_0(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  5 && (K+L) >= 1) {
        h2_5_1(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  5 && (K+L) >= 2) {
        h2_5_2(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  5 && (K+L) >= 3) {
        h2_5_3(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  5 && (K+L) >= 4) {
        h2_5_4(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
#ifdef CUDA_SPDF
    if ((I+J) >=  6 && (K+L) >= 0) {
        h2_6_0(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  6 && (K+L) >= 1) {
        h2_6_1(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  6 && (K+L) >= 2) {
        h2_6_2(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    if ((I+J) >=  6 && (K+L) >= 3) {
        h2_6_3(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
#endif
}


