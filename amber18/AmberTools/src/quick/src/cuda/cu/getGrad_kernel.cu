

#ifdef int_spd
__global__ void getGrad_kernel()
#elif defined int_spdf
__global__ void getGrad_kernel_spdf()
#elif defined int_spdf2
__global__ void getGrad_kernel_spdf2()
#elif defined int_spdf3
__global__ void getGrad_kernel_spdf3()
#elif defined int_spdf4
__global__ void getGrad_kernel_spdf4()
#elif defined int_spdf5
__global__ void getGrad_kernel_spdf5()
#elif defined int_spdf6
__global__ void getGrad_kernel_spdf6()
#elif defined int_spdf7
__global__ void getGrad_kernel_spdf7()
#elif defined int_spdf8
__global__ void getGrad_kernel_spdf8()
#endif
{
    unsigned int offside = blockIdx.x*blockDim.x+threadIdx.x;
    int totalThreads = blockDim.x*gridDim.x;
    
    
    QUICKULL jshell = (QUICKULL) devSim.sqrQshell;
    QUICKULL jshell2 = (QUICKULL) devSim.sqrQshell;
    
    for (QUICKULL i = offside; i<jshell2*jshell; i+= totalThreads) {
        
        /*
        QUICKULL a, b;
        
        // That's simply because no sqrt for ULL
        double aa = (double)((i+1)*1E-4);
        QUICKULL t = (QUICKULL)(sqrt(aa)*1E2);
        
        
        if ((i+1)==t*t) {
            t--;
        }
        
        QUICKULL k = i-t*t;
        if (k<=t) {
            a = k;
            b = t;
        }else {
            a = t;
            b = 2*t-k;
        }
        */
        
        QUICKULL a = (QUICKULL) i/jshell;
        QUICKULL b = (QUICKULL) (i - a*jshell);
        
        
        int II = devSim.sorted_YCutoffIJ[a].x;
        int KK = devSim.sorted_YCutoffIJ[b].x;
        
        int ii = devSim.sorted_Q[II];
        int kk = devSim.sorted_Q[KK];
        
        if (ii<=kk){
            
            int JJ = devSim.sorted_YCutoffIJ[a].y;
            int LL = devSim.sorted_YCutoffIJ[b].y;
            
            int jj = devSim.sorted_Q[JJ];
            int ll = devSim.sorted_Q[LL];
            
            
            if ( !((devSim.katom[ii] == devSim.katom[jj]) &&
                   (devSim.katom[ii] == devSim.katom[kk]) &&
                   (devSim.katom[ii] == devSim.katom[ll]))     // In case 4 indices are in the same atom
                ) {
                
                int nshell = devSim.nshell;
                
                QUICKDouble DNMax = MAX(MAX(4.0*LOC2(devSim.cutMatrix, ii, jj, nshell, nshell), 4.0*LOC2(devSim.cutMatrix, kk, ll, nshell, nshell)),
                                        MAX(MAX(LOC2(devSim.cutMatrix, ii, ll, nshell, nshell),     LOC2(devSim.cutMatrix, ii, kk, nshell, nshell)),
                                            MAX(LOC2(devSim.cutMatrix, jj, kk, nshell, nshell),     LOC2(devSim.cutMatrix, jj, ll, nshell, nshell))));
                
                
                if ((LOC2(devSim.YCutoff, kk, ll, nshell, nshell) * LOC2(devSim.YCutoff, ii, jj, nshell, nshell))> devSim.integralCutoff && \
                    (LOC2(devSim.YCutoff, kk, ll, nshell, nshell) * LOC2(devSim.YCutoff, ii, jj, nshell, nshell) * DNMax) > devSim.integralCutoff) {
                    
                    int iii = devSim.sorted_Qnumber[II];
                    int jjj = devSim.sorted_Qnumber[JJ];
                    int kkk = devSim.sorted_Qnumber[KK];
                    int lll = devSim.sorted_Qnumber[LL];
#ifdef int_spd
                    iclass_grad(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#elif defined int_spdf
                    if ( (kkk + lll) >= 4 ) {
                        iclass_grad_spdf(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
                    }
#elif defined int_spdf2
                    if ( (iii + jjj) >= 4 ) {
                        iclass_grad_spdf2(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
                    }
#elif defined int_spdf3
                    iclass_grad_spdf3(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#elif defined int_spdf4
                    iclass_grad_spdf4(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#elif defined int_spdf5
                    iclass_grad_spdf5(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#elif defined int_spdf6
                    iclass_grad_spdf6(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#elif defined int_spdf7
                    iclass_grad_spdf7(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#elif defined int_spdf8
                    iclass_grad_spdf8(iii, jjj, kkk, lll, ii, jj, kk, ll, DNMax);
#endif
                    
                }
            }
        }
    }
}

