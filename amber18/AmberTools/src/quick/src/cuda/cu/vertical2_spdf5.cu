
__device__ void vertical2_spdf5(int I, int J, int K, int L,
                                QUICKDouble* YVerticalTemp, QUICKDouble* store,
                                QUICKDouble Ptempx, QUICKDouble Ptempy, QUICKDouble Ptempz,  \
                                QUICKDouble WPtempx,QUICKDouble WPtempy,QUICKDouble WPtempz, \
                                QUICKDouble Qtempx, QUICKDouble Qtempy, QUICKDouble Qtempz,  \
                                QUICKDouble WQtempx,QUICKDouble WQtempy,QUICKDouble WQtempz, \
                                QUICKDouble ABCDtemp,QUICKDouble ABtemp, \
                                QUICKDouble CDtemp, QUICKDouble ABcom, QUICKDouble CDcom)
{
    
    if ((I+J) >=  4 && (K+L) >= 6) {
         h2_4_6(YVerticalTemp, store, \
        Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
        WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
}


