
__device__ __forceinline__ int lefthrr(QUICKDouble RAx, QUICKDouble RAy, QUICKDouble RAz,
                                       QUICKDouble RBx, QUICKDouble RBy, QUICKDouble RBz,
                                       int KLMNAx, int KLMNAy, int KLMNAz,
                                       int KLMNBx, int KLMNBy, int KLMNBz,
                                       int IJTYPE,QUICKDouble* coefAngularL, int* angularL)
{
    
    
    int numAngularL;
    
    coefAngularL[0] = 1.0;
    angularL[0] = (int) LOC3(devTrans, KLMNAx + KLMNBx, KLMNAy + KLMNBy, KLMNAz + KLMNBz, TRANSDIM, TRANSDIM, TRANSDIM);
    
    if (IJTYPE == 0) {
        numAngularL = 1;
        return numAngularL;
    }
    else if (IJTYPE == 1)
    {
        numAngularL = 2;
        
        if (KLMNBx != 0) {
            coefAngularL[1] = RAx-RBx;
        }else if(KLMNBy !=0 ){
            coefAngularL[1] = RAy-RBy;
        }else if (KLMNBz != 0) {
            coefAngularL[1] = RAz-RBz;
        }
        
        angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        
        return numAngularL;
    }
    else if (IJTYPE == 2)
    {
        
        if (KLMNBx == 2 || KLMNBy == 2 || KLMNBz == 2) {
            numAngularL = 3;
            QUICKDouble tmp;
            
            
            if (KLMNBx == 2) {
                tmp = RAx - RBx;
                angularL[1] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else if(KLMNBy == 2) {
                tmp = RAy - RBy;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else if (KLMNBz == 2 ){
                tmp = RAz - RBz;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            coefAngularL[1] = 2 * tmp;
            coefAngularL[2]= tmp * tmp;
            
            
            angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            
            return numAngularL;
            
        }else{
            
            numAngularL = 4;
            QUICKDouble tmp, tmp2;
            
            if(KLMNBx == 1 && KLMNBy == 1){
                tmp = RAx - RBx;
                tmp2 = RAy - RBy;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
                
            }else if (KLMNBx == 1 && KLMNBz == 1) {
                tmp = RAx - RBx;
                tmp2 = RAz - RBz;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else if (KLMNBy == 1 && KLMNBz == 1) {
                tmp = RAy - RBy;
                tmp2 = RAz - RBz;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            
            coefAngularL[1] = tmp;
            coefAngularL[2] = tmp2;
            coefAngularL[3] = tmp * tmp2;
            
            
            angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            
            return numAngularL;
        }
    }
    else if (IJTYPE == 3)
    {
        if (KLMNBx == 3 || KLMNBy == 3 || KLMNBz == 3) {
            numAngularL = 4;
            QUICKDouble tmp;
            
            if (KLMNBx == 3) {
                tmp = RAx - RBx;
                angularL[1] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else if (KLMNBy == 3) {
                tmp = RAy - RBy;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else if (KLMNBz == 3) {
                tmp = RAz - RBz;
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            
            coefAngularL[1] = 3 * tmp;
            coefAngularL[2] = 3 * tmp * tmp;
            coefAngularL[3] = tmp * tmp * tmp;
            
            
            angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            
            return numAngularL;
        }else if (KLMNBx == 1 && KLMNBy == 1) {
            numAngularL = 8;
            QUICKDouble tmp = RAx - RBx;
            QUICKDouble tmp2 = RAy - RBy;
            QUICKDouble tmp3 = RAz - RBz;
            
            coefAngularL[1] = tmp;
            coefAngularL[2] = tmp2;
            coefAngularL[3] = tmp3;
            coefAngularL[4] = tmp * tmp2;
            coefAngularL[5] = tmp * tmp3;
            coefAngularL[6] = tmp2 * tmp3;
            coefAngularL[7] = tmp * tmp2 * tmp3;
            
            angularL[1] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            angularL[3] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
            angularL[4] = (int) LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            angularL[5] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
            angularL[6] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
            
            angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            
            return numAngularL;
        }else{
            
            numAngularL = 6;
            QUICKDouble tmp;
            QUICKDouble tmp2;
            
            if (KLMNBx == 1) {
                tmp = RAx - RBx;
            }else if (KLMNBy == 1){
                tmp = RAy - RBy;
            }else if (KLMNBz == 1){
                tmp = RAz - RBz;
            }
            
            if (KLMNBx == 2) {
                tmp2 = RAx - RBx;
            }else if (KLMNBy == 2){
                tmp2 = RAy - RBy;
            }else if (KLMNBz == 2){
                tmp2 = RAz - RBz;
            }
            
            coefAngularL[1] = tmp;
            coefAngularL[2] = 2 * tmp2;
            coefAngularL[3] = 2 * tmp * tmp2;
            coefAngularL[4] = tmp2 * tmp2;
            coefAngularL[5] = tmp * tmp2 * tmp2;
            
            
            if (KLMNBx == 2) {
                angularL[1] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[3] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            if (KLMNBy == 2) {
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[3] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            if (KLMNBz == 2) {
                angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
                angularL[3] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            if (KLMNBx == 1) {
                if (KLMNBy == 2) {  //120
                    angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
                }else{              //102
                    angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
                }
            }
            
            if (KLMNBy == 1) {
                if (KLMNBx == 2) {  // 210
                    angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
                }else{              // 012
                    angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
                }
            }
            
            if (KLMNBz == 1) {
                if (KLMNBx == 2) {  // 201
                    angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
                }else{              // 021
                    angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
                }
            }
            
            
            if (KLMNBx == 1) {
                angularL[4] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            if (KLMNBy == 1) {
                angularL[4] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            if (KLMNBz == 1) {
                angularL[4] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
            
            angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            
            return numAngularL;
            
            
            
        }
    }    
    
    /*    else if (IJTYPE == 4)
     {
     if (KLMNBx == 4) {
     numAngularL = 5;
     QUICKDouble tmp = RAx - RBx;
     
     coefAngularL[1] = 4 * tmp;
     coefAngularL[2] = 6 * tmp * tmp;
     coefAngularL[3] = 4 * tmp * tmp * tmp;
     coefAngularL[4] = tmp * tmp * tmp * tmp;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx+3, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBy == 4) {
     numAngularL = 5;
     QUICKDouble tmp = RAy - RBy;
     coefAngularL[1] = 4 * tmp;
     coefAngularL[2] = 6 * tmp * tmp;
     coefAngularL[3] = 4 * tmp * tmp * tmp;
     coefAngularL[4] = tmp * tmp * tmp * tmp;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+3, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     
     }else if (KLMNBz == 4) {
     numAngularL = 5;
     
     QUICKDouble tmp = RAz - RBz;
     coefAngularL[1] = 4 * tmp;
     coefAngularL[2] = 6 * tmp * tmp;
     coefAngularL[3] = 4 * tmp * tmp * tmp;
     coefAngularL[4] = tmp * tmp * tmp * tmp;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+3, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     
     }else if (KLMNBx == 1 && KLMNBy == 3) {
     numAngularL = 8;
     QUICKDouble tmp = RAx - RBx;
     QUICKDouble tmp2 = RAy - RBy;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = 3 * tmp2;
     coefAngularL[3] = 3 * tmp * tmp2;
     coefAngularL[4] = 3 * tmp2 * tmp2;
     coefAngularL[5] = 3 * tmp * tmp2 * tmp2;
     coefAngularL[6] = tmp2 * tmp2 * tmp2;
     coefAngularL[7] = tmp * tmp2 * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+3, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBx == 3 && KLMNBy == 1) {
     numAngularL = 8;
     QUICKDouble tmp = RAy - RBy;
     QUICKDouble tmp2 = RAx - RBx;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = 3 * tmp2;
     coefAngularL[3] = 3 * tmp * tmp2;
     coefAngularL[4] = 3 * tmp2 * tmp2;
     coefAngularL[5] = 3 * tmp * tmp2 * tmp2;
     coefAngularL[6] = tmp2 * tmp2 * tmp2;
     coefAngularL[7] = tmp * tmp2 * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx+3, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     }
     
     else if (KLMNBx == 1 && KLMNBz ==3) {
     numAngularL = 8;
     QUICKDouble tmp = RAx - RBx;
     QUICKDouble tmp2 = RAz - RBz;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = 3 * tmp2;
     coefAngularL[3] = 3 * tmp * tmp2;
     coefAngularL[4] = 3 * tmp2 * tmp2;
     coefAngularL[5] = 3 * tmp * tmp2 * tmp2;
     coefAngularL[6] = tmp2 * tmp2 * tmp2;
     coefAngularL[7] = tmp * tmp2 * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx,   KLMNAy, KLMNAz+3, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx,   KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx,   KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     
     }else if (KLMNBx == 3 && KLMNBz == 1) {
     numAngularL = 8;
     QUICKDouble tmp = RAz - RBz;
     QUICKDouble tmp2 = RAx - RBx;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = 3 * tmp2;
     coefAngularL[3] = 3 * tmp * tmp2;
     coefAngularL[4] = 3 * tmp2 * tmp2;
     coefAngularL[5] = 3 * tmp * tmp2 * tmp2;
     coefAngularL[6] = tmp2 * tmp2 * tmp2;
     coefAngularL[7] = tmp * tmp2 * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx+3, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx,   KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     
     }else if (KLMNBy == 1 && KLMNBz == 3) {
     numAngularL = 8;
     QUICKDouble tmp = RAy - RBy;
     QUICKDouble tmp2 = RAz - RBz;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = 3 * tmp2;
     coefAngularL[3] = 3 * tmp * tmp2;
     coefAngularL[4] = 3 * tmp2 * tmp2;
     coefAngularL[5] = 3 * tmp * tmp2 * tmp2;
     coefAngularL[6] = tmp2 * tmp2 * tmp2;
     coefAngularL[7] = tmp * tmp2 * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+3, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     
     }else if (KLMNBy == 3 && KLMNBz == 1) {
     numAngularL = 8;
     QUICKDouble tmp = RAz - RBz;
     QUICKDouble tmp2 = RAy - RBy;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = 3 * tmp2;
     coefAngularL[3] = 3 * tmp * tmp2;
     coefAngularL[4] = 3 * tmp2 * tmp2;
     coefAngularL[5] = 3 * tmp * tmp2 * tmp2;
     coefAngularL[6] = tmp2 * tmp2 * tmp2;
     coefAngularL[7] = tmp * tmp2 * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+3, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     
     }else if (KLMNBx == 2 && KLMNBy == 2) {
     numAngularL = 9;
     QUICKDouble tmp = RAx - RBx;
     QUICKDouble tmp2 = RAy - RBy;
     
     coefAngularL[1] = 2 * tmp;
     coefAngularL[2] = 2 * tmp2;
     coefAngularL[3] = 4 * tmp * tmp2;
     coefAngularL[4] = tmp * tmp;
     coefAngularL[5] = tmp2 * tmp2;
     coefAngularL[6] = 2 * tmp * tmp2 * tmp2;
     coefAngularL[7] = 2 * tmp * tmp * tmp2;
     coefAngularL[8] = tmp * tmp * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[7] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBx == 2 && KLMNBz == 2) {
     numAngularL = 9;
     QUICKDouble tmp = RAx - RBx;
     QUICKDouble tmp2 = RAz - RBz;
     
     coefAngularL[1] = 2 * tmp;
     coefAngularL[2] = 2 * tmp2;
     coefAngularL[3] = 4 * tmp * tmp2;
     coefAngularL[4] = tmp * tmp;
     coefAngularL[5] = tmp2 * tmp2;
     coefAngularL[6] = 2 * tmp * tmp2 * tmp2;
     coefAngularL[7] = 2 * tmp * tmp * tmp2;
     coefAngularL[8] = tmp * tmp * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx,   KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[7] = (int) LOC3(devTrans, KLMNAx,   KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBy == 2 && KLMNBz == 2) {
     numAngularL = 9;
     QUICKDouble tmp = RAy - RBy;
     QUICKDouble tmp2 = RAz - RBz;
     
     coefAngularL[1] = 2 * tmp;
     coefAngularL[2] = 2 * tmp2;
     coefAngularL[3] = 4 * tmp * tmp2;
     coefAngularL[4] = tmp * tmp;
     coefAngularL[5] = tmp2 * tmp2;
     coefAngularL[6] = 2 * tmp * tmp2 * tmp2;
     coefAngularL[7] = 2 * tmp * tmp * tmp2;
     coefAngularL[8] = tmp * tmp * tmp2 * tmp2;
     
     angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[7] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBx == 1 && KLMNBy == 1 && KLMNBz == 2) {
     numAngularL = 12;
     QUICKDouble tmp = RAx - RBx;
     QUICKDouble tmp2 = RAy - RBy;
     QUICKDouble tmp3 = RAz - RBz;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = tmp2;
     coefAngularL[3] = 2 * tmp3;
     coefAngularL[4] = tmp * tmp2;
     coefAngularL[5] = 2 * tmp * tmp3;
     coefAngularL[6] = 2 * tmp2 * tmp3;
     coefAngularL[7] = tmp3 * tmp3;
     coefAngularL[8] = 2 * tmp * tmp2 * tmp3;
     coefAngularL[9] = tmp * tmp3 * tmp3;
     coefAngularL[10] = tmp2 * tmp3 * tmp3;
     coefAngularL[11] = tmp * tmp2 * tmp3 * tmp3;
     
     angularL[1] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[7] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[8] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[9] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[10] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBx == 1 && KLMNBz == 1 && KLMNBy == 2) {
     numAngularL = 12;
     QUICKDouble tmp = RAx - RBx;
     QUICKDouble tmp2 = RAz - RBz;
     QUICKDouble tmp3 = RAy - RBy;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = tmp2;
     coefAngularL[3] = 2 * tmp3;
     coefAngularL[4] = tmp * tmp2;
     coefAngularL[5] = 2 * tmp * tmp3;
     coefAngularL[6] = 2 * tmp2 * tmp3;
     coefAngularL[7] = tmp3 * tmp3;
     coefAngularL[8] = 2 * tmp * tmp2 * tmp3;
     coefAngularL[9] = tmp * tmp3 * tmp3;
     coefAngularL[10] = tmp2 * tmp3 * tmp3;
     coefAngularL[11] = tmp * tmp2 * tmp3 * tmp3;
     
     angularL[1] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+2, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+2, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+2, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[7] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[8] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[9] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[10] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     }else if (KLMNBy == 1 && KLMNBz == 1 && KLMNBx == 2) {
     numAngularL = 12;
     QUICKDouble tmp = RAy - RBy;
     QUICKDouble tmp2 = RAz - RBz;
     QUICKDouble tmp3 = RAx - RBx;
     
     coefAngularL[1] = tmp;
     coefAngularL[2] = tmp2;
     coefAngularL[3] = 2 * tmp3;
     coefAngularL[4] = tmp * tmp2;
     coefAngularL[5] = 2 * tmp * tmp3;
     coefAngularL[6] = 2 * tmp2 * tmp3;
     coefAngularL[7] = tmp3 * tmp3;
     coefAngularL[8] = 2 * tmp * tmp2 * tmp3;
     coefAngularL[9] = tmp * tmp3 * tmp3;
     coefAngularL[10] = tmp2 * tmp3 * tmp3;
     coefAngularL[11] = tmp * tmp2 * tmp3 * tmp3;
     
     angularL[1] = (int)  LOC3(devTrans, KLMNAx+2, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[2] = (int)  LOC3(devTrans, KLMNAx+2, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[3] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[4] = (int)  LOC3(devTrans, KLMNAx+2, KLMNAy,   KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[5] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[6] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[7] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[8] = (int)  LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[9] = (int)  LOC3(devTrans, KLMNAx,   KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
     angularL[10] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
     }
     }
     
     */
    //angularL[numAngularL - 1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
    
    return numAngularL;
}

