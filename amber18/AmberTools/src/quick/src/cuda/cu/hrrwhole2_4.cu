
__device__ __forceinline__ QUICKDouble hrrwhole2_4(int I, int J, int K, int L, \
                                                   int III, int JJJ, int KKK, int LLL, int IJKLTYPE, QUICKDouble* store, \
                                                   QUICKDouble RAx,QUICKDouble RAy,QUICKDouble RAz, \
                                                   QUICKDouble RBx,QUICKDouble RBy,QUICKDouble RBz, \
                                                   QUICKDouble RCx,QUICKDouble RCy,QUICKDouble RCz, \
                                                   QUICKDouble RDx,QUICKDouble RDy,QUICKDouble RDz)
{
    /*
     When this subroutine is called, only (ij|kl) k+l=5 and i+j=6 integral is computed, but (k+l)>=5 and i+j=6 is entering this subroutine
     since i+j = 6, i=3 and j= 3
     so if (k+l) = 5, then, the highest integral is used, and no selection hrr.
     if (k+l) = 6, then, k=3 and l=3
     */
    
    QUICKDouble Y = 0.0;
    
    if ((K+L)== 5) // k+l = 5, and i = 3 and j = 3
    {
        
        Y = LOC2(store,  \
                 (int) LOC3(devTrans, LOC2(devSim.KLMN,0,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,0,JJJ-1,3,devSim.nbasis), \
                            LOC2(devSim.KLMN,1,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,1,JJJ-1,3,devSim.nbasis), \
                            LOC2(devSim.KLMN,2,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,2,JJJ-1,3,devSim.nbasis), TRANSDIM, TRANSDIM, TRANSDIM)-1,
                 (int) LOC3(devTrans, LOC2(devSim.KLMN,0,KKK-1,3,devSim.nbasis) + LOC2(devSim.KLMN,0,LLL-1,3,devSim.nbasis), \
                            LOC2(devSim.KLMN,1,KKK-1,3,devSim.nbasis) + LOC2(devSim.KLMN,1,LLL-1,3,devSim.nbasis), \
                            LOC2(devSim.KLMN,2,KKK-1,3,devSim.nbasis) + LOC2(devSim.KLMN,2,LLL-1,3,devSim.nbasis), TRANSDIM, TRANSDIM, TRANSDIM)-1 , \
                 STOREDIM, STOREDIM) * devSim.cons[III-1] * devSim.cons[JJJ-1] * devSim.cons[KKK-1] * devSim.cons[LLL-1];
        return Y;
        
    }else{ //k=3 and l = 3, for i and j , i = 3 and j = 3
        int angularR[12];
        QUICKDouble coefAngularR[12];
        
        
        // For hrr, only k+l need hrr, but can be simplified to only consider k+l=5 contibution
        int numAngularR = lefthrr(RCx, RCy, RCz, RDx, RDy, RDz,
                                  LOC2(devSim.KLMN,0,KKK-1,3,devSim.nbasis), LOC2(devSim.KLMN,1,KKK-1,3,devSim.nbasis), LOC2(devSim.KLMN,2,KKK-1,3,devSim.nbasis),
                                  LOC2(devSim.KLMN,0,LLL-1,3,devSim.nbasis), LOC2(devSim.KLMN,1,LLL-1,3,devSim.nbasis), LOC2(devSim.KLMN,2,LLL-1,3,devSim.nbasis),
                                  3, coefAngularR, angularR);
        
        for (int j = 0; j<numAngularR; j++) {
            if (angularR[j] <= STOREDIM ) {
                Y += coefAngularR[j] * LOC2(store,
                                            (int) LOC3(devTrans, LOC2(devSim.KLMN,0,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,0,JJJ-1,3,devSim.nbasis), \
                                                       LOC2(devSim.KLMN,1,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,1,JJJ-1,3,devSim.nbasis), \
                                                       LOC2(devSim.KLMN,2,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,2,JJJ-1,3,devSim.nbasis), TRANSDIM, TRANSDIM, TRANSDIM)-1, \
                                            angularR[j]-1 , STOREDIM, STOREDIM);
            }
        }
        
        
        Y = Y * devSim.cons[III-1] * devSim.cons[JJJ-1] * devSim.cons[KKK-1] * devSim.cons[LLL-1];
    }
    return Y;
}
