
__device__ __forceinline__ QUICKDouble hrrwhole2_6(int I, int J, int K, int L, \
                                                   int III, int JJJ, int KKK, int LLL, int IJKLTYPE, QUICKDouble* store, \
                                                   QUICKDouble RAx,QUICKDouble RAy,QUICKDouble RAz, \
                                                   QUICKDouble RBx,QUICKDouble RBy,QUICKDouble RBz, \
                                                   QUICKDouble RCx,QUICKDouble RCy,QUICKDouble RCz, \
                                                   QUICKDouble RDx,QUICKDouble RDy,QUICKDouble RDz)
{
    /*
     When this subroutine is called, (ij|kl) where i+j = 6 and k+l = 4 is computed, but (i+j) ==6 and k+l >= 4 entering this subroutine
     therefore, i = 3 and j = 3 is confirmed.
     */
    
    if ((K+L)== 4 && (I+J)==6) // k+l = 4, and i+j = 6
    {
        
        QUICKDouble Y = LOC2(store,  \
                             (int) LOC3(devTrans, LOC2(devSim.KLMN,0,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,0,JJJ-1,3,devSim.nbasis), \
                                        LOC2(devSim.KLMN,1,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,1,JJJ-1,3,devSim.nbasis), \
                                        LOC2(devSim.KLMN,2,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,2,JJJ-1,3,devSim.nbasis), TRANSDIM, TRANSDIM, TRANSDIM)-1,
                             (int) LOC3(devTrans, LOC2(devSim.KLMN,0,KKK-1,3,devSim.nbasis) + LOC2(devSim.KLMN,0,LLL-1,3,devSim.nbasis), \
                                        LOC2(devSim.KLMN,1,KKK-1,3,devSim.nbasis) + LOC2(devSim.KLMN,1,LLL-1,3,devSim.nbasis), \
                                        LOC2(devSim.KLMN,2,KKK-1,3,devSim.nbasis) + LOC2(devSim.KLMN,2,LLL-1,3,devSim.nbasis), TRANSDIM, TRANSDIM, TRANSDIM)-1 , \
                             STOREDIM, STOREDIM) * devSim.cons[III-1] * devSim.cons[JJJ-1] * devSim.cons[KKK-1] * devSim.cons[LLL-1];
        return Y;
        
    }
    
    int angularR[12];
    QUICKDouble coefAngularR[12];
    QUICKDouble Y = 0.0;
    
    // For hrr, only k+l need hrr, but can be simplified to only consider k+l=5 contibution
    int numAngularR = lefthrr(RCx, RCy, RCz, RDx, RDy, RDz,
                              LOC2(devSim.KLMN,0,KKK-1,3,devSim.nbasis), LOC2(devSim.KLMN,1,KKK-1,3,devSim.nbasis), LOC2(devSim.KLMN,2,KKK-1,3,devSim.nbasis),
                              LOC2(devSim.KLMN,0,LLL-1,3,devSim.nbasis), LOC2(devSim.KLMN,1,LLL-1,3,devSim.nbasis), LOC2(devSim.KLMN,2,LLL-1,3,devSim.nbasis),
                              L, coefAngularR, angularR);
    
    for (int j = 0; j<numAngularR; j++) {
        if (angularR[j] <= STOREDIM ) {
            Y += coefAngularR[j] * LOC2(store,
                                        (int) LOC3(devTrans, LOC2(devSim.KLMN,0,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,0,JJJ-1,3,devSim.nbasis), \
                                                   LOC2(devSim.KLMN,1,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,1,JJJ-1,3,devSim.nbasis), \
                                                   LOC2(devSim.KLMN,2,III-1,3,devSim.nbasis) + LOC2(devSim.KLMN,2,JJJ-1,3,devSim.nbasis), TRANSDIM, TRANSDIM, TRANSDIM)-1, \
                                        angularR[j]-1 , STOREDIM, STOREDIM);
        }
    }
    
    
    Y = Y * devSim.cons[III-1] * devSim.cons[JJJ-1] * devSim.cons[KKK-1] * devSim.cons[LLL-1];
    
    return Y;
    
}
