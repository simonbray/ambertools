
__device__ __forceinline__ int lefthrr23_new(QUICKDouble RAx, QUICKDouble RAy, QUICKDouble RAz,
                                         QUICKDouble RBx, QUICKDouble RBy, QUICKDouble RBz,
                                         int KLMNAx, int KLMNAy, int KLMNAz,
                                         int KLMNBx, int KLMNBy, int KLMNBz,
                                         int IJTYPE,QUICKDouble* coefAngularL, int* angularL)
{
    
    coefAngularL[0] = 1.0;
    angularL[0] = (int) LOC3(devTrans, KLMNAx + KLMNBx, KLMNAy + KLMNBy, KLMNAz + KLMNBz, TRANSDIM, TRANSDIM, TRANSDIM);
    
    
    /*
     if this subroutine is called, (ij|kl) for (k+l)>=5 is computed, but (k+l)>=5 entering this subroutine
     here ijtype is the value of l
     */
    //if (IJTYPE == 2) // If l = 2, then k = 3
    //{
    //    return 1;
    //}
    //else if (IJTYPE == 3) // If l = 3, then k = 2 or 3, only k+l = 5 or 6 is valid, so only keep k+2 items
    //{
    coefAngularL[0] = 1.0;
    angularL[0] = (int) LOC3(devTrans, KLMNAx + KLMNBx, KLMNAy + KLMNBy, KLMNAz + KLMNBz, TRANSDIM, TRANSDIM, TRANSDIM);
    
    QUICKDouble tmp4 = 1.0;
    
    QUICKDouble tmpx = RAx - RBx;
    QUICKDouble tmpy = RAy - RBy;
    QUICKDouble tmpz = RAz - RBz;
    
    if (KLMNBx > 0) {
        tmp4 = tmp4 * quick_pow(tmpx, KLMNBx);
    }
    
    if (KLMNBy > 0) {
        tmp4 = tmp4 * quick_pow(tmpy, KLMNBy);
    }
    
    if (KLMNBz > 0) {
        tmp4 = tmp4 * quick_pow(tmpz, KLMNBz);
    }
    
    
    int numAngularL = 1;
    
    if (KLMNBx >= 2 && tmpx != 0) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBx * (KLMNBx - 1) / 2 * tmp4 / (tmpx * tmpx);
        numAngularL++;
    }
    
    
    if (KLMNBy >= 2 && tmpy != 0) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBy * (KLMNBy - 1) / 2 * tmp4 / (tmpy * tmpy);
        numAngularL++;
    }
    
    if (KLMNBz >= 2 && tmpz != 0) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBz * (KLMNBz - 1) / 2 * tmp4 / (tmpz * tmpz);
        numAngularL++;
    }
    
    if (KLMNBx >= 1 && KLMNBy >= 1 && tmpx != 0 && tmpy != 0) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBx * KLMNBy * tmp4 / (tmpx * tmpy);
        numAngularL++;
    }
    
    if (KLMNBx >= 1 && KLMNBz >= 1 && tmpx != 0 && tmpz != 0) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBx * KLMNBz * tmp4 / (tmpx * tmpz);
        numAngularL++;
    }
    
    
    if (KLMNBy >= 1 && KLMNBz >= 1 && tmpy != 0 && tmpz != 0) {
        angularL[numAngularL] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        coefAngularL[numAngularL] = (QUICKDouble) KLMNBy * KLMNBz * tmp4 / (tmpy * tmpz);
        numAngularL++;
    }
    
    /* the last case is IJTYPE = 3 */
    return numAngularL;
    
    //return 0;
}
