
__device__ void vertical2_spdf9(int I, int J, int K, int L,
                                QUICKDouble* YVerticalTemp, QUICKDouble* store,
                                QUICKDouble Ptempx, QUICKDouble Ptempy, QUICKDouble Ptempz,  \
                                QUICKDouble WPtempx,QUICKDouble WPtempy,QUICKDouble WPtempz, \
                                QUICKDouble Qtempx, QUICKDouble Qtempy, QUICKDouble Qtempz,  \
                                QUICKDouble WQtempx,QUICKDouble WQtempy,QUICKDouble WQtempz, \
                                QUICKDouble ABCDtemp,QUICKDouble ABtemp, \
                                QUICKDouble CDtemp, QUICKDouble ABcom, QUICKDouble CDcom)
{
    if ((I+J) >=  7 && (K+L) >= 0) {
        h2_7_0(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    
    if ((I+J) >=  7 && (K+L) >= 1) {
        h2_7_1(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    
    if ((I+J) >=  7 && (K+L) >= 2) {
        h2_7_2(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    
    if ((I+J) >=  7 && (K+L) >= 3) {
        h2_7_3(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
    
    if ((I+J) >=  7 && (K+L) >= 4) {
        h2_7_4(YVerticalTemp, store, \
               Ptempx, Ptempy, Ptempz, WPtempx, WPtempy, WPtempz, Qtempx, Qtempy, Qtempz,  \
               WQtempx, WQtempy, WQtempz, ABCDtemp, ABtemp, CDtemp,  ABcom, CDcom);
    }
}


