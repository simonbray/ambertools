
__device__ __forceinline__ int lefthrr23(QUICKDouble RAx, QUICKDouble RAy, QUICKDouble RAz,
                                    QUICKDouble RBx, QUICKDouble RBy, QUICKDouble RBz,
                                    int KLMNAx, int KLMNAy, int KLMNAz,
                                    int KLMNBx, int KLMNBy, int KLMNBz,
                                    int IJTYPE,QUICKDouble* coefAngularL, int* angularL)
{
    int numAngularL;
    
    coefAngularL[0] = 1.0;
    angularL[0] = (int) LOC3(devTrans, KLMNAx + KLMNBx, KLMNAy + KLMNBy, KLMNAz + KLMNBz, TRANSDIM, TRANSDIM, TRANSDIM);
    
    
    /*
     if this subroutine is called, (ij|kl) for (k+l)>=5 is computed, but (k+l)>=5 entering this subroutine
     here ijtype is the value of l
     */
    //if (IJTYPE == 2) // If l = 2, then k = 3
    //{
    //    return 1;
    //}
    //else if (IJTYPE == 3) // If l = 3, then k = 2 or 3, only k+l = 5 or 6 is valid, so only keep k+2 items
    //{
    if (KLMNBx == 3 || KLMNBy == 3 || KLMNBz == 3) {
        numAngularL = 2;
        QUICKDouble tmp;
        
        if (KLMNBx == 3) {
            tmp = RAx - RBx;
            angularL[1] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        }else if (KLMNBy == 3) {
            tmp = RAy - RBy;
            angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
        }else if (KLMNBz == 3) {
            tmp = RAz - RBz;
            angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy, KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
        }
        
        
        coefAngularL[1] = 3 * tmp;
        
        return numAngularL;
    }else if (KLMNBx == 1 && KLMNBy == 1) {
        numAngularL = 4;
        QUICKDouble tmp = RAx - RBx;
        QUICKDouble tmp2 = RAy - RBy;
        QUICKDouble tmp3 = RAz - RBz;
        
        coefAngularL[1] = tmp;
        coefAngularL[2] = tmp2;
        coefAngularL[3] = tmp3;
        
        angularL[1] = (int) LOC3(devTrans, KLMNAx,   KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy,   KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
        angularL[3] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
        
        
        return numAngularL;
    }else{
        
        numAngularL = 3;
        QUICKDouble tmp;
        QUICKDouble tmp2;
        
        if (KLMNBx == 1) {
            tmp = RAx - RBx;
        }else if (KLMNBy == 1){
            tmp = RAy - RBy;
        }else if (KLMNBz == 1){
            tmp = RAz - RBz;
        }
        
        if (KLMNBx == 2) {
            tmp2 = RAx - RBx;
        }else if (KLMNBy == 2){
            tmp2 = RAy - RBy;
        }else if (KLMNBz == 2){
            tmp2 = RAz - RBz;
        }
        
        coefAngularL[1] = tmp;
        coefAngularL[2] = 2 * tmp2;
        
        
        if (KLMNBx == 2) {
            angularL[1] = (int) LOC3(devTrans, KLMNAx+2, KLMNAy, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
        }
        
        if (KLMNBy == 2) {
            angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy+2, KLMNAz,   TRANSDIM, TRANSDIM, TRANSDIM);
        }
        
        if (KLMNBz == 2) {
            angularL[1] = (int) LOC3(devTrans, KLMNAx, KLMNAy,   KLMNAz+2, TRANSDIM, TRANSDIM, TRANSDIM);
        }
        
        if (KLMNBx == 1) {
            if (KLMNBy == 2) {  //120
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else{              //102
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
        }
        
        if (KLMNBy == 1) {
            if (KLMNBx == 2) {  // 210
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy+1, KLMNAz, TRANSDIM, TRANSDIM, TRANSDIM);
            }else{              // 012
                angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
        }
        
        if (KLMNBz == 1) {
            if (KLMNBx == 2) {  // 201
                angularL[2] = (int) LOC3(devTrans, KLMNAx+1, KLMNAy, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }else{              // 021
                angularL[2] = (int) LOC3(devTrans, KLMNAx, KLMNAy+1, KLMNAz+1, TRANSDIM, TRANSDIM, TRANSDIM);
            }
        }
        
        
        
        return numAngularL;
        
        
        
        //}
        
    }
    
    //return 0;
}


