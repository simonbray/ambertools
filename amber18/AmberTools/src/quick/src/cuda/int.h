#undef STOREDIM
#define STOREDIM STOREDIM_L

#include "include/h_all_files.h"
#include "include/h_all_subroutines.h"

#include "cu/vertical_spdf.cu"
#include "cu/vertical2_spdf.cu"
#include "cu/vertical2_spdf2.cu"
#include "cu/vertical2_spdf3.cu"
#include "cu/vertical2_spdf4.cu"
#include "cu/vertical2_spdf5.cu"
#include "cu/vertical2_spdf6.cu"
#include "cu/vertical2_spdf7.cu"
#include "cu/vertical2_spdf8.cu"
#include "cu/vertical2_spdf9.cu"
#include "cu/vertical3.cu"

#undef STOREDIM
#define STOREDIM STOREDIM_S

#include "cu/vertical.cu"
#include "cu/vertical2.cu"


#undef STOREDIM
