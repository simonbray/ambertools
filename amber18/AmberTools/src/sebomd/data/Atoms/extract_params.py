#!/usr/bin/env python

import sys
import math

Angstrom_per_Bohr = 0.529177249
eV_per_Hartree = 27.21
#eV_per_Hartree = 27.2113962

def dichotomie(f,a,b,eps):
  if f(a)*f(b) > 0.:
    raise RuntimeError, "Error in dichotomie: f(%f)*f(%f) = %f (should be < 0.)" % (a,b,f(a)*f(b))
  m1 = a
  m2 = b
  while (math.fabs(m2-m1) > eps):
    c = 0.5*(m1+m2)
    if f(m1)*f(c) < 0.:
      m1 = m1
      m2 = c
    else:
      m1 = c
      m2 = m2
  return m1

class Atom(object):
  PeriodicTable = {
"H":  {'name': "Hydrogen",    'Z' :  1, 'n' : 1, 's' : 1, 'p' : 0, 'heat' :  52.102},
"C":  {'name': "Carbon",      'Z' :  6, 'n' : 2, 's' : 2, 'p' : 2, 'heat' : 170.890},
"N":  {'name': "Nitrogen",    'Z' :  7, 'n' : 2, 's' : 2, 'p' : 3, 'heat' : 113.000},
"O":  {'name': "Oxygen",      'Z' :  8, 'n' : 2, 's' : 2, 'p' : 4, 'heat' :  59.559},
"F":  {'name': "Fluorine",    'Z' :  9, 'n' : 2, 's' : 2, 'p' : 5, 'heat' :  18.890},
"Si": {'name': "Silicon",     'Z' : 14, 'n' : 3, 's' : 2, 'p' : 2, 'heat' : 108.390},
"P":  {'name': "Phosphorous", 'Z' : 15, 'n' : 3, 's' : 2, 'p' : 3, 'heat' :  75.570},
"S":  {'name': "Sulfur",      'Z' : 16, 'n' : 3, 's' : 2, 'p' : 4, 'heat' :  66.400},
"Cl": {'name': "Chloride",    'Z' : 17, 'n' : 3, 's' : 2, 'p' : 5, 'heat' :  28.990},
"Br": {'name': "Bromide",     'Z' : 35, 'n' : 4, 's' : 2, 'p' : 5, 'heat' :  26.740},
"I":  {'name': "Iodine",      'Z' : 53, 'n' : 5, 's' : 2, 'p' : 5, 'heat' :  25.517},
}

  principalQuantumNumbers = {
'H'  :  1,
'C'  :  2,
'N'  :  2,
'O'  :  2,
'F'  :  2,
'Si' :  3,
'P'  :  3,
'S'  :  3,
'Cl' :  3,
'Br' :  4,
'I'  :  5,
}
  def __init__(self,element):
    self.element = element
    self.atomicNumber = Atom.PeriodicTable[element]['Z']
    self.n = Atom.PeriodicTable[element]['n'] # principal quantum number
    self.s = Atom.PeriodicTable[element]['s'] # s orbital occupation number
    self.p = Atom.PeriodicTable[element]['p'] # p orbital occupation number
    self.data = {}

  def D1(self):
     n = self.principalQuantumNumbers[self.element]
     zs = float(self.data['ZS'])
     zp = float(self.data['ZP'])
     num = (2*n+1)*(4.*zs*zp)**(n+0.5)
     den = math.sqrt(3)*(zs+zp)**(2*n+2)
     d1 = num/den # a.u.
     return d1 # in bohr
#    return "%15.8f" % d1 # in bohr

  def D2(self):
     n = self.principalQuantumNumbers[self.element]
     zp = float(self.data['ZP'])
     try:
       d2 = math.sqrt((4*n*n+6*n+2)/20.)/zp
     except:
       d2 = 0.0
     return d2 # in bohr
#    return "%15.8f" % d2 # in bohr

  def AM(self):
    gss = float(self.data['GSS'])
#   am = gss/eV_per_Hartree # hartree

    # from A. A. Bliznyuk, A. A. Voityuk J. Struct. Chem. 28, 312-314 (1987)
    # MOPAC:
    rho0 = eV_per_Hartree/2./gss
    # Divcon:
    am = 0.5/rho0
#   mopac = float(self.data['PO1'])
#   print "AM: %2s: MOPAC = %15.8f HERE = %15.8f DIFF= %15.8f" % (self.element, mopac, 0.5/am, mopac-0.5/am)

    return am
#   return "%15.8f" % am

  def AD(self):
    hsp = float(self.data['HSP'])/eV_per_Hartree # to hartree
    d1 = self.D1()

    def f(rho):
      # from A. A. Bliznyuk, A. A. Voityuk J. Struct. Chem. 28, 312-314 (1987)
      return 1/rho-1/math.sqrt(d1*d1+rho*rho)-hsp*4.
    rho1 = dichotomie(f,1e-8,10.0,1e-9)
    ad = 0.5/rho1

#   mopac = float(self.data['PO2'])
#   print mopac, f(mopac), f(1e-8), f(2.0)
#   print "AD: %2s: MOPAC = %15.8f HERE = %15.8f DIFF= %15.8f" % (self.element, mopac, 0.5/ad, mopac-0.5/ad)

    return ad
#   return "%15.8f" % ad

  def AQ(self):
    gpp = float(self.data['GPP'])/eV_per_Hartree # to hartree
    gp2 = float(self.data['GP2'])/eV_per_Hartree # to hartree
    hpp = 0.5*(gpp-gp2)
    d2 = self.D2()

    def f(rho):
      # from A. A. Bliznyuk, A. A. Voityuk J. Struct. Chem. 28, 312-314 (1987)
      return (1/rho+1/math.sqrt(rho*rho+2*d2*d2)-2/math.sqrt(rho*rho+d2*d2))-hpp*8.
      
    rho2 = dichotomie(f,1e-8,10.,1e-9)
    aq = 0.5/rho2

#   mopac = float(self.data['PO3'])
#   print hpp, mopac, f(mopac), f(1e-8), f(10.)
#   print "AQ: %2s: MOPAC = %15.8f HERE = %15.8f DIFF= %15.8f" % (self.element, mopac, 0.5/aq, mopac-0.5/aq)
    return aq

  def Energy(self):
    Uss = float(self.data['USS'])
    Gss = float(self.data['GSS'])
    try:
      Upp = float(self.data['UPP'])
      Gpp = float(self.data['GPP'])
      Gsp = float(self.data['GSP'])
      Gp2 = float(self.data['GP2'])
      Hsp = float(self.data['HSP'])
    except:
      Upp = 0.0
      Gpp = 0.0
      Gsp = 0.0
      Gp2 = 0.0
      Hsp = 0.0
    nrj = Uss*self.s +       \
          Upp*self.p +       \
          Gss*max(self.s-1,0) +  \
          Gsp*self.s*self.p + \
          Gp2*((self.p * (self.p-1))/2 + 0.5*(min(self.p,6-self.p)*((min(self.p,6-self.p)-1))/2)) +\
          Gpp*(-0.5*(min(self.p,6-self.p)*((min(self.p,6-self.p)-1))/2)) +\
          Hsp*(-self.p)
    mopac = float(self.data['EISOL'])
    print "NRJ: %2s: MOPAC = %15.8f HERE = %15.8f DIFF= %15.8f" % (self.element, mopac, nrj, mopac-nrj)
    return nrj


  def __repr__(self):
    if self.method == 'MNDO':
      return self.__reprMNDO__(1)
    elif self.method == 'AM1':
      return self.__reprAM1__(2)
    elif self.method == 'PM3':
      return self.__reprAM1__(3) # same representation as AM1
#   elif ....
#     block=4 is for user input
    elif self.method == 'RM1':
      return self.__reprAM1__(5) # same representation as AM1
#   elif self.method == 'AM1/dPhoT': not available in MOPAC
#     return self.__reprAM1__(6)
#   elif self.method == 'PM3/PDDG': not available in MOPAC
#     return self.__reprAM1__(7)
    elif self.method == 'AM1/d-CB1': # not available in MOPAC
      return self.__reprAM1__(8)   # same representation as AM1
    elif self.method == 'PM6':
      return self.__reprPM6__(9)
    elif self.method == 'PM7':
      return self.__reprPM6__(10)
    else:
      raise RuntimeError, 'No definition for the current method: %s' % self.method

  def __reprU__(self, block):
    """ U_{ss} and U_{pp} """
    myrepr = ""
    if 'UPP' not in self.data: self.data['UPP'] = '0.00000000'
    myrepr += "      DATA UCORE%d(0,%2d)   /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['USS'])
    myrepr += "      DATA UCORE%d(1,%2d)   /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['UPP'])
    return myrepr

  def __reprZeta__(self, block):
    """ \zeta_s and \zeta_p """
    myrepr = ""
    if 'ZP' not in self.data: self.data['ZP'] = '0.00000000'
    myrepr += "      DATA EXPNT%d(0,%2d)   /%15sD0/ ! 1/bohr\n" % (block,self.atomicNumber, self.data['ZS'])
    myrepr += "      DATA EXPNT%d(1,%2d)   /%15sD0/ ! 1/bohr\n" % (block,self.atomicNumber, self.data['ZP'])
    return myrepr

  def __reprBeta__(self, block):
    """ \beta_s and \beta_p """
    myrepr = ""
    if 'BETAP' not in self.data: self.data['BETAP'] = '0.00000000'
    myrepr += "      DATA BETA%d(0,%2d)    /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['BETAS'])
    myrepr += "      DATA BETA%d(1,%2d)    /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['BETAP'])
    return myrepr

  def __reprMonoelec__(self, block):
    # G_{ss}, G_{sp}, G_{pp},  G_{p2},  H_{sp}, 
    myrepr = ""
    if 'GPP' not in self.data: self.data['GPP'] = '0.00000000'
    if 'GSP' not in self.data: self.data['GSP'] = '0.00000000'
    if 'GP2' not in self.data: self.data['GP2'] = '0.00000000'
    if 'HSP' not in self.data: self.data['HSP'] = '0.00000000'
    myrepr += "      DATA GSS%d(%2d)       /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['GSS'])
    myrepr += "      DATA GPP%d(%2d)       /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['GPP'])
    myrepr += "      DATA GSP%d(%2d)       /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['GSP'])
    myrepr += "      DATA GP2%d(%2d)       /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['GP2'])
    myrepr += "      DATA HSP%d(%2d)       /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['HSP'])
    return myrepr

  def __reprBielec__(self, block):
    # \rho_0, \rho_1, \rho_2, D_1, D_2
    myrepr = ""
    myrepr += "      DATA AL%d(0,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, self.AM())
    if float(self.data['ZP']) > 0.0:
      myrepr += "      DATA AL%d(1,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, self.AD())
      myrepr += "      DATA AL%d(2,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, self.AQ())
      myrepr += "      DATA DL%d(1,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, self.D1())
      myrepr += "      DATA DL%d(2,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, self.D2())
    else:
      myrepr += "      DATA AL%d(1,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, 0.0)
      myrepr += "      DATA AL%d(2,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, 0.0)
      myrepr += "      DATA DL%d(1,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, 0.0)
      myrepr += "      DATA DL%d(2,%2d)      /%15.8fD0/ ! bohr\n" % (block,self.atomicNumber, 0.0)
    return myrepr

  def __reprEnergies__(self, block):
    myrepr = ""
    # Total energy of the atom
#   myrepr += "      DATA EEATM%d(%2d)     /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['EISOL'])
    myrepr += "      DATA EEATM%d(%2d)     /%15.8fD0/ ! eV\n" % (block,self.atomicNumber, self.Energy())
    # Heat of formation
    myrepr += "      DATA HFATM%d(%2d)     /%15sD0/ ! kcal/mol\n" % (block,self.atomicNumber, self.data['EHEAT'])
    return myrepr

  def __reprGaussianAM1__(self,block):
    myrepr = ""
    # K, L, M core-core
    if 'FN11' not in self.data: self.data['FN11'] = '0.00000000'
    if 'FN12' not in self.data: self.data['FN12'] = '0.00000000'
    if 'FN13' not in self.data: self.data['FN13'] = '0.00000000'
    if 'FN14' not in self.data: self.data['FN14'] = '0.00000000'
    if 'FN21' not in self.data: self.data['FN21'] = '0.00000000'
    if 'FN22' not in self.data: self.data['FN22'] = '0.00000000'
    if 'FN23' not in self.data: self.data['FN23'] = '0.00000000'
    if 'FN24' not in self.data: self.data['FN24'] = '0.00000000'
    if 'FN31' not in self.data: self.data['FN31'] = '0.00000000'
    if 'FN32' not in self.data: self.data['FN32'] = '0.00000000'
    if 'FN33' not in self.data: self.data['FN33'] = '0.00000000'
    if 'FN34' not in self.data: self.data['FN34'] = '0.00000000'
    myrepr += "      DATA AGAUS%d(1,%2d)   /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['FN11'])
    myrepr += "      DATA AGAUS%d(2,%2d)   /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['FN12'])
    myrepr += "      DATA AGAUS%d(3,%2d)   /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['FN13'])
    myrepr += "      DATA AGAUS%d(4,%2d)   /%15sD0/ ! eV\n" % (block,self.atomicNumber, self.data['FN14'])
    myrepr += "      DATA BGAUS%d(1,%2d)   /%15sD0/ ! 1/Angstroms**2\n" % (block,self.atomicNumber, self.data['FN21'])
    myrepr += "      DATA BGAUS%d(2,%2d)   /%15sD0/ ! 1/Angstroms**2\n" % (block,self.atomicNumber, self.data['FN22'])
    myrepr += "      DATA BGAUS%d(3,%2d)   /%15sD0/ ! 1/Angstroms**2\n" % (block,self.atomicNumber, self.data['FN23'])
    myrepr += "      DATA BGAUS%d(4,%2d)   /%15sD0/ ! 1/Angstroms**2\n" % (block,self.atomicNumber, self.data['FN24'])
    myrepr += "      DATA CGAUS%d(1,%2d)   /%15sD0/ ! Angstroms\n" % (block,self.atomicNumber, self.data['FN31'])
    myrepr += "      DATA CGAUS%d(2,%2d)   /%15sD0/ ! Angstroms\n" % (block,self.atomicNumber, self.data['FN32'])
    myrepr += "      DATA CGAUS%d(3,%2d)   /%15sD0/ ! Angstroms\n" % (block,self.atomicNumber, self.data['FN33'])
    myrepr += "      DATA CGAUS%d(4,%2d)   /%15sD0/ ! Angstroms\n" % (block,self.atomicNumber, self.data['FN34'])

    return myrepr

  def __reprCommon__(self,block):
    myrepr = ""
    myrepr+= """!
! %s: %s: %s
!
""" % (self.element, self.method, self.reference)

    myrepr += self.__reprU__(block)
    myrepr += self.__reprZeta__(block)
    myrepr += self.__reprBeta__(block)
    myrepr += self.__reprMonoelec__(block)
    myrepr += self.__reprBielec__(block)
    myrepr += self.__reprEnergies__(block)
    return myrepr

  def __reprMNDO__(self,block):
    myrepr = self.__reprCommon__(block)

    # \alpha
    myrepr += "      DATA ACORE%d(%2d)     /%15sD0/ ! 1/Angstroms\n" % (block,self.atomicNumber, self.data['ALP'])

    return myrepr
  
  def __reprAM1__(self,block):
    myrepr = self.__reprCommon__(block)
    # \alpha
    myrepr += "      DATA ACORE%d(%2d)     /%15sD0/ ! 1/Angstroms\n" % (block,self.atomicNumber, self.data['ALP'])
    myrepr += self.__reprGaussianAM1__(block)
    return myrepr
  
  def __reprPM6__(self,block):
    myrepr = self.__reprCommon__(block)
    return myrepr
  
  def __reprPM7__(self,block):
    myrepr = self.__reprCommon__(block)
    return myrepr

mopacOut=sys.argv[1]
mopac=open(mopacOut, 'r')
paramOut=sys.argv[2]
param=open(paramOut, 'w')

atoms = {}
Found = False
for line in mopac:
  # search for atom references (initialization of the atoms dict)
  if line[3:4] == ':':
    data = line.split(':')
    element = data[0].strip()
    method = data[1].strip().replace('(','').replace(')','')
    reference = data[2].strip()
    atom = Atom(element)
    atom.method = method
    atom.reference = reference
    atoms[atom.atomicNumber] = atom
  if line.startswith(' PARAMETER VALUES USED IN THE CALCULATION'):
    Found = True
    line = mopac.next()
    line = mopac.next()
    line = mopac.next()
  if 'ONE-ELECTRON' in line:
    Found = False
  if Found:
    data = line.split()
    if len(data) > 0:
      if int(data[0]) in atoms:
        atoms[int(data[0])].data[data[1]] = data[2]
#   line.next()
#   line.next()
#   line.next()
#   line.next()
#   line.next()

param.write("!==============================================\n")
param.write("! Data extracted from MOPAC 2012 (HCORE option)\n")
param.write("!==============================================\n")
atomKeys = atoms.keys()
atomKeys.sort()
for atom in atomKeys:
  param.write(repr(atoms[atom]))

#print repr(atoms[6])
#print repr(atoms[1])
#print atoms[1].AM()
#print atoms[1].data['PO1']
#print atoms[8].AM()
#print atoms[9].AM()
#print atoms[8].AD()
#print atoms[9].AD()
#print atoms[6].AQ()
#print atoms[7].AQ()
#print atoms[8].AQ()
#print atoms[9].AQ()
#print atoms[6].data['GSS'], atoms[7].data['GSS'], atoms[8].data['GSS'], atoms[9].data['GSS']
#print atoms[6].data['GSP'], atoms[7].data['GSP'], atoms[8].data['GSP'], atoms[9].data['GSP']
#print atoms[6].data['GPP'], atoms[7].data['GPP'], atoms[8].data['GPP'], atoms[9].data['GPP']
#print atoms[6].data['GP2'], atoms[7].data['GP2'], atoms[8].data['GP2'], atoms[9].data['GP2']
#print atoms[6].data['HSP'], atoms[7].data['HSP'], atoms[8].data['HSP'], atoms[9].data['HSP']
