set(XPM_SOURCES data.c create.c misc.c rgb.c scan.c parse.c hashtable.c
	XpmCrBufFrI.c XpmCrDataFrP.c XpmCrPFrBuf.c XpmRdFToI.c XpmWrFFrI.c
	XpmCrBufFrP.c XpmCrIFrBuf.c XpmCrPFrData.c XpmRdFToP.c XpmWrFFrP.c
	XpmCrDataFrI.c XpmCrIFrData.c XpmRdFToData.c XpmWrFFrData.c)

add_compile_options(${NO_OPT_CFLAGS})

add_library(Xpm STATIC ${XPM_SOURCES})
