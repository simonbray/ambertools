set(XRAW_SOURCES 3d.c 	AllWidgets.c 	Arrow.c 	AsciiSink.c 	AsciiSrc.c 
	AsciiText.c 	Box.c 	Clock.c 	Command.c 	color.c 	Container.c 	Dialog.c
	Form.c 	Frame.c 	Grip.c 	Label.c 	List.c 	Logo.c 	MenuButton.c
	Paned.c 	Panner.c 	Porthole.c 	Repeater.c 	Scrollbar.c 	ScrolledTable.c
	Separator.c 	Simple.c 	SimpleMenu.c 	Sme.c 	SmeBSB.c 	SmeLine.c 	
	StripChart.c 	table.c 	Table2.c 	Text.c 	TextSink.c 	TextSrc.c 	TextAction.c 	
	TextPop.c 	TextTr.c 	Toggle.c 	Tree.c 	Viewport.c 	Vendor.c 	XawInit.c)
	
add_compile_options(${NO_OPT_CFLAGS})

add_library(Xaw STATIC ${XRAW_SOURCES})
