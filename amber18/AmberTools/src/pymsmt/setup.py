
from distutils.core import setup
import os
import sys

# Packages in python MSMT toolbox
packages = ['pymsmt', 'pymsmt.api', 'pymsmt.mcpb', 'pymsmt.mol', 'pymsmt.ipmach']

# Modules
modules = ['pymsmt.exp', 'pymsmt.title', 'pymsmt.lib']

# Scripts
scripts = ['pymsmt/tools/MCPB.py', 'pymsmt/tools/OptC4.py', 'pymsmt/tools/PdbSearcher.py',
           'pymsmt/tools/espgen.py', 'pymsmt/tools/CartHess2FC.py', 'pymsmt/tools/IPMach.py',
           'pymsmt/tools/car_to_files.py', 'pymsmt/tools/ProScrs.py', 'pymsmt/tools/mol2rtf.py',
           'pymsmt/tools/amb2chm_psf_crd.py', 'pymsmt/tools/amb2chm_par.py', 'pymsmt/tools/amb2gro_top_gro.py']

if __name__ == '__main__':

    try:
        from distutils.command.build_py import build_py_2to3 as build_py
        from distutils.command.build_scripts import build_scripts_2to3 as build_scripts
        PY3 = True
    except ImportError:
        from distutils.command.build_py import build_py
        from distutils.command.build_scripts import build_scripts
        PY3 = False

    setup(name='pyMSMT',
          version='19.0', # For AmberTools 19
          description='python Metal Site Modeling Toolbox',
          author='Pengfei Li',
          author_email='ldsoar1990 -at- gmail.com',
          license='GPL v3 or later',
          packages=packages,
          py_modules=modules,
          cmdclass={'build_py':build_py, 'build_scripts':build_scripts},
          scripts=scripts)
