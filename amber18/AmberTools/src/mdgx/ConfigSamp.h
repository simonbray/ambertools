#ifndef ConfigFunctions
#define ConfigFunctions

#include "ConfigSampDS.h"
#include "TrajectoryDS.h"

void InitConfigs(configs *cfsinp);

void SampleConfigurations(configs *cfsinp, trajcon *tj, prmtop *tp);

#endif
