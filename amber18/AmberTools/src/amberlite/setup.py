
from distutils.core import setup
import os
import sys

scripts = ['pymdpbsa', 'pytleap']

if __name__ == '__main__':
    try:
        from distutils.command.build_py import build_py_2to3 as build_py
        from distutils.command.build_scripts import build_scripts_2to3 as build_scripts
        PY3 = True
    except ImportError:
        from distutils.command.build_py import build_py
        from distutils.command.build_scripts import build_scripts
        PY3 = False

    setup(name='AmberLite',
          version='16.0',
          description='Various modules needed for AmberTools Python programs',
          author='Romain Wolf, Pawel Janowski, and Jason M. Swails',
          author_email='jason.swails -at- gmail.com',
          url='http://ambermd.org',
          license='GPL v2 or later',
          packages=[], py_modules=[],
          cmdclass={'build_py': build_py, 'build_scripts': build_scripts},
          scripts=scripts)
