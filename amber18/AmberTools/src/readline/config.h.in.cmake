/* config.h.in for the CMake build system.  Maintained by hand. */

/* Define NO_MULTIBYTE_SUPPORT to not compile in support for multibyte
   characters, even if the OS supports them. */
#undef NO_MULTIBYTE_SUPPORT

/* Define if on MINIX.  */
#undef _MINIX

/* Define as the return type of signal handlers (int or void).
  Apparantly with remotely modern systems this is always void*/
#define RETSIGTYPE void

#define VOID_SIGHANDLER 1

#define PROTOTYPES

#cmakedefine __CHAR_UNSIGNED__

/* Define if the `S_IS*' macros in <sys/stat.h> do not work properly.  */
#cmakedefine STAT_MACROS_BROKEN

/* Define if you have the isascii function. */
#cmakedefine HAVE_ISASCII

/* Define if you have the isxdigit function. */
#cmakedefine HAVE_ISXDIGIT

/* Define if you have the lstat function. */
#cmakedefine HAVE_LSTAT

/* Define if you have the mbrlen function. */
#cmakedefine HAVE_MBRLEN

/* Define if you have the mbrtowc function. */
#cmakedefine HAVE_MBRTOWC

/* Define if you have the mbrtowc function. */
#cmakedefine HAVE_MBRTOWC

/* Define if you have the mbsrtowcs function. */
#cmakedefine HAVE_MBSRTOWCS

/* Define if you have the memmove function. */
#cmakedefine HAVE_MEMMOVE

/* Define if you have the putenv function.  */
#cmakedefine HAVE_PUTENV

/* Define if you have the select function.  */
#cmakedefine HAVE_SELECT

/* Define if you have the setenv function.  */
#cmakedefine HAVE_SETENV

/* Define if you have the setlocale function. */
#cmakedefine HAVE_SETLOCALE

/* Define if you have the strcasecmp function.  */
#cmakedefine HAVE_STRCASECMP

/* Define if you have the strcoll function.  */
#cmakedefine HAVE_STRCOLL

#cmakedefine STRCOLL_BROKEN

/* Define if you have the strpbrk function.  */
#cmakedefine HAVE_STRPBRK

/* Define if you have the tcgetattr function.  */
#cmakedefine HAVE_TCGETATTR

/* Define if you have the vsnprintf function.  */
#cmakedefine HAVE_VSNPRINTF

/* Define if you have the wctomb function.  */
#cmakedefine HAVE_WCTOMB 

/* Define if you have the wcwidth function.  */
#cmakedefine HAVE_WCWIDTH

#define STDC_HEADERS

/* Define if you have the <dirent.h> header file.  */
#cmakedefine HAVE_DIRENT_H

/* Define if you have the <langinfo.h> header file.  */
#cmakedefine HAVE_LANGINFO_H

/* Define if you have the <limits.h> header file.  */
#cmakedefine HAVE_LIMITS_H

/* Define if you have the <locale.h> header file.  */
#cmakedefine HAVE_LOCALE_H

/* Define if you have the <memory.h> header file.  */
#cmakedefine HAVE_MEMORY_H

/* Define if you have the <ndir.h> header file.  */
#cmakedefine HAVE_NDIR_H

/* Define if you have the <stdarg.h> header file.  */
#cmakedefine HAVE_STDARG_H

/* Define if you have the <stdlib.h> header file.  */
#cmakedefine HAVE_STDLIB_H

/* Define if you have the <string.h> header file.  */
#cmakedefine HAVE_STRING_H

/* Define if you have the <strings.h> header file.  */
#cmakedefine HAVE_STRINGS_H

/* Define if you have the <sys/dir.h> header file.  */
#cmakedefine HAVE_SYS_DIR_H

/* Define if you have the <sys/file.h> header file.  */
#cmakedefine HAVE_SYS_FILE_H

/* Define if you have the <sys/ndir.h> header file.  */
#cmakedefine HAVE_SYS_NDIR_H

/* Define if you have the <sys/pte.h> header file.  */
#cmakedefine HAVE_SYS_PTE_H

/* Define if you have the <sys/ptem.h> header file.  */
#cmakedefine HAVE_SYS_PTEM_H

/* Define if you have the <sys/select.h> header file.  */
#cmakedefine HAVE_SYS_SELECT_H

/* Define if you have the <sys/stream.h> header file.  */
#cmakedefine HAVE_SYS_STREAM_H

/* Define if you have the <termcap.h> header file.  */
#cmakedefine HAVE_TERMCAP_H

/* Define if you have the <termio.h> header file.  */
#cmakedefine HAVE_TERMIO_H

/* Define if you have the <termios.h> header file.  */
#cmakedefine HAVE_TERMIOS_H

/* Define if you have the <unistd.h> header file.  */
#cmakedefine HAVE_UNISTD_H

/* Define if you have the <varargs.h> header file.  */
#cmakedefine HAVE_VARARGS_H

/* Define if you have the <wchar.h> header file.  */
#cmakedefine HAVE_WCHAR_H

/* Define if you have the <wctype.h> header file.  */
#cmakedefine HAVE_WCTYPE_H

#cmakedefine HAVE_MBSTATE_T

/* Define if you have <langinfo.h> and nl_langinfo(CODESET). */
#cmakedefine HAVE_LANGINFO_CODESET

#cmakedefine HAVE_BSD_SIGNALS 1

#cmakedefine HAVE_POSIX_SIGNALS 1

#cmakedefine HAVE_USG_SIGHOLD 1

// TODO: finish configure script and stop using these precalculated defines
#cmakedefine GWINSZ_IN_SYS_IOCTL 1

#define STRUCT_WINSIZE_IN_SYS_IOCTL 1

/* #undef STRUCT_WINSIZE_IN_TERMIOS */

#cmakedefine TIOCSTAT_IN_SYS_IOCTL 1

#cmakedefine FIONREAD_IN_SYS_IOCTL 1

/* #undef SPEED_T_IN_SYS_TYPES */

#define HAVE_GETPW_DECLS 1

/* #undef STRUCT_DIRENT_HAS_D_INO */

/* #undef STRUCT_DIRENT_HAS_D_FILENO */

/* #undef MUST_REINSTALL_SIGHANDLERS */

#define CTYPE_NON_ASCII 1

//end predefined area

/* modify settings or make new ones based on what autoconf tells us. */

/* Ultrix botches type-ahead when switching from canonical to
   non-canonical mode, at least through version 4.3 */
#if !defined (HAVE_TERMIOS_H) || !defined (HAVE_TCGETATTR) || defined (ultrix)
#  define TERMIOS_MISSING
#endif

#if defined (STRCOLL_BROKEN)
#  undef HAVE_STRCOLL
#endif

#if defined (__STDC__) && defined (HAVE_STDARG_H)
#  define PREFER_STDARG
#  define USE_VARARGS
#else
#  if defined (HAVE_VARARGS_H)
#    define PREFER_VARARGS
#    define USE_VARARGS
#  endif
#endif
