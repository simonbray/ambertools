/*
 * Tests for SysV-style signals
 * Stolen from aclocal.m4
 */

#include <signal.h>

void foo() { }

void main()
{
    int mask = sigmask(SIGINT);
    sigset(SIGINT, foo);
    sigrelse(SIGINT);
    sighold(SIGINT);
    sigpause(SIGINT);
}