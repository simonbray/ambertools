/*
 * Code to check the signedness of char.  If char is unsigned, the array size will be negative and it will fail to compile.
 * This code stolen from the configure script
 */

int main()
{
    static int test_array [1 - 2 * !(((char) -1) < 0)];
    test_array [0] = 0;
    return 0;
}