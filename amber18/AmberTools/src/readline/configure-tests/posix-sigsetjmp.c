#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <setjmp.h>

int main()
{
    #if !defined (_POSIX_VERSION) || !defined (HAVE_POSIX_SIGNALS)
        exit (1);
    #else

    int code;
    sigset_t set, oset;
    sigjmp_buf xx;

    /* get the mask */
    sigemptyset(&set);
    sigemptyset(&oset);
    sigprocmask(SIG_BLOCK, (sigset_t *)NULL, &set);
    sigprocmask(SIG_BLOCK, (sigset_t *)NULL, &oset);

    /* save it */
    code = sigsetjmp(xx, 1);
    if (code)
      exit(0);	/* could get sigmask and compare to oset here. */

    /* change it */
    sigaddset(&set, SIGINT);
    sigprocmask(SIG_BLOCK, &set, (sigset_t *)NULL);

    /* and siglongjmp */
    siglongjmp(xx, 10);
    exit(1);
    #endif
}