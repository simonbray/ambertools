/*
 * Tests for BSD-style signals
 * Stolen from aclocal.m4
 */

#include <signal.h>

void main()
{
    int mask = sigmask(SIGINT);
    sigsetmask(mask);
    sigblock(mask);
    sigpause(mask);
}