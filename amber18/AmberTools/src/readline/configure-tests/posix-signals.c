/*
 * Tests for POSIX-style signals
 * Stolen from aclocal.m4
 */

#define _POSIX_C_SOURCE 199309L

#include <signal.h>

void main()
{
    sigset_t ss;
    struct sigaction sa;
    sigemptyset(&ss);
    sigsuspend(&ss);
    sigaction(SIGINT, &sa, (struct sigaction *) 0);
    sigprocmask(SIG_BLOCK, &ss, (sigset_t *) 0);
}