#!/usr/bin/env python

import os, sys, math, subprocess, random, argparse, shutil, atexit, signal, logging
import tarfile
script_path = os.path.abspath(os.path.dirname(__file__))+os.path.sep
sys.path.append(script_path)
from lib.measure_parms import *
from lib.amber import *
from lib import tqdm
from lib.pdbremix.lib.docopt import docopt
from lib.pdbremix import pdbatoms
from lib.pdbremix.rmsd import *
from lib.pdbremix.volume import volume
from lib.charmmlipid2amber.charmmlipid2amber import charmmlipid2amber

head = """

 _____           _                    _
|  __ \         | |                  | |
| |__) |_ _  ___| | ___ __ ___   ___ | |
|  ___/ _` |/ __| |/ / '_ ` _ \ / _ \| |
| |  | (_| | (__|   <| | | | | | (_) | |
|_|   \__,_|\___|_|\_\_| |_| |_|\___/|_|
                                         ___
                  /\/\   ___ _ __ ___   / _ \___ _ __
                 /    \ / _ \ '_ ` _ \ / /_\/ _ \ '_ \ 
                / /\/\ \  __/ | | | | / /_ \  __/ | | |
                \/    \/\___|_| |_| |_\____/\___|_| |_|


###############################################################
Stephan Schott-Verdugo 2016-11-07 UPDATED: 2019-04-07   v1.0.5
Generated at CPCLab at Heinrich Heine University Duesseldorf
###############################################################\n"""
#CHANGELOG
#v1.0.5:
#-Changed pdb file distro
#-Added experimental pdbs for all possible combinations
#-Added PI & multiple protonation states parameters
#-memgen.parm to all missing lipids
#v1.0.2:
#-Fixed the way dims and solute_inmem are handled
#-Included parameters for lysophospholipid heads PE PG PC // BETA! (pdbs and memgen.parm)
#-Fixed and updated internal charmmlipid2amber.csv
#-Added missing MG pdb
#v1.0.1:
#-Added flags dims, gaff2 and solute_charge
#-Possible to specify concentrations as percentages. Depends on pdbremix at the moment! 
#v1.0.0:
#-Modified POPC pdb to avoid bug that apparently caused it to be stuck in a maxima
#-Fixed bug while trying to find lipid APL with the same headgroup
#-Newest packmol version 18.169
#-Inclusion of boost 1.69 // compatible with gcc 8 compilers  // AmberTools19 includes boost!
#-Mac compatible compilation code 
#v0.9.95:
#-Added writeout flag to control intermediate saves. Allows shorter nloops.
#-Adapted code to Leandro's nloop_all. 
#-New packmol version with bug fixes (18.104). Should fix isues with Windows WSL.
#-Python 3 friendly code.
#-Added cubic flag for solvating. reducebuild for protonating HIS.
#-Fixed multi bilayer z-dim size bug.
#-Changed solute_con to accept either number of molecules or concentrations.
#v0.9.9:
#-Possibility to build stacked membrane systems by calling --lipids multiple times. PDB files to be embeded have to be included with --pdb now, to make possible to specify multiple inputs.
#-A charge imbalance can be specified between compartments (CompEL!)
#-If desired, a specific per leaflet composition can be specified.
#-The orientation by MEMEMBED is with the N-terminus to the "inside" (or to lower z values). Added flag (--n_ter) to set this per protein in case of multiple bilayers.
#v0.9.8:
#-Modified print commands for logging statements.
#-Added optional grid building command (testing!)
#-Added functions that allow minimization and parametrization, given that AMBERHOME is defined
#-Added option to keep ligands after protein alignment
#-Updated progress bar
#v0.9.7:
#-Check if ions are required before adding the line to packmol (avoids "bug" that adds one molecule even when the number is 0)
#v0.9.6:
#-Possible to add solutes in water based on PDB input
#-Modified sigterm command for runs
#-Reduced the verbosity. Can be called with --verbose
#v0.9.5:
#-Changed parsing system to argparse
#-Adapted to work with Wooey
#-Implemented charmmlipid2amber as a module // charmm and amber outputs
#v0.9.1:
#-Now is possible to create membranes without proteins by setting the PDB as None
#-The xy dimensions can be set with the flag -distxy_fix. This flag is required if no PDB is given
#v0.9:
#-Multiple lipids available!
#-Detection of neutralization / salt concentration included
#v0.7:
#-Volume estimation by building grid based on PDBREMIX
#-Search for AMBERHOME definition, using reduce if available
#-Automated alignment available based on MEMEMBED
#-Tiny code clean-up
#v0.3:
#-Distance is measured considering worst case scenario (max x and y considered to be on the diagonal)
#-Added progress bar for all-together packing step
#-Filter for non-defined arguments

explanation = """The script creates an input file for PACKMOL for creating a bilayer system with a protein inserted in it. The input pdb file will be protonated and oriented by default by using reduce and MEMEMBED; the user is encouraged to check the input and output files carefully!  If the protein is preoriented, for example by using PPM webserver from OPM (http://opm.phar.umich.edu/server.php), be sure to set the corresponding flag (--preoriented).  In some cases the packed system might crash during the first MD step, specially when the system is minimized by using pmemd GPU code (cudaFree Memory allocation error). It's highly recommended that the minimization steps are performed on cpus (--minimize), but also changes in the box boundaries or repacking with --random as an argument might help.

 If you use this script, you should cite and/or acknowledge:

    **Leandro Martinez (L. Martinez, R. Andrade, E. G. Birgin, J. M. Martinez. Packmol: A package for building initial configurations for molecular dynamics simulations. Journal of Computational Chemistry, 30(13):2157-2164, 2009.)
    **Tim Nugent (Nugent T, Jones DT. Membrane protein orientation and refinement using a knowledge-based statistical potential. BMC Bioinformatics. 2013 Sep 18;14:276.)
    **Bosco K. Ho (http://boscoh.com/ ; https://github.com/boscoh/pdbremix)
    **Benjamin D. Madej (charmmlipid2amber.py) """

parser = argparse.ArgumentParser(prog="packmol-memgen", description = explanation)
parser.add_argument("--available_lipids",action="store_true",     help="list of available lipids and corresponding charges")
parser.add_argument("--available_lipids_all",action="store_true", help="list all lipids including experimental. Huge output (~1600)!")
parser.add_argument("--lipids",  action="append",                 help="Lipid(s) to be used for embeding the protein. It should be a single string separated by ':' . If different composition is used in leaflets, add '//' as a separator.[ex. CHL1:DOPC//DOPE for a lower leaflet with CHL1+DOPC and an upper leaflet with DOPE]. Can be defined multiple times for multi-bilayer systems (stacks 'up' or 'outside')")
parser.add_argument("--ratio",   action="append",                 help="mixture ratio (set to 1 if only one lipid required). Must be in the same order and syntax as in lipids, and defined once per bilayer [ex. 1:2//1] ")
parser.add_argument("--dist", type=float, default=15.0,           help="specify the minimum distance between the maxmin values for x y and z to the box boundaries. Default = 15 A. Worst case scenario is considered, so real distance could be larger")
parser.add_argument("--dist_wat", type=float, default=17.5,       help="specify the width of the water layer over the membrane or protein in the z axis. Default = 17.5")
parser.add_argument("--distxy_fix", type=float,                   help="specify a predefined distance between the maxmin values for x and y to the box boundaries. By default is calculated flexibly for each system.")
parser.add_argument("--dims", nargs=3,type=float,default=[0,0,0], help="box dimensions vector for the  x y z  axes. Be sure to use dimensions that cover the complete protein to be packed!!")
parser.add_argument("--solvate",         action="store_true",     help="solvate the system without adding lipids. Disables the flag --dist_wat, using only --dist to set the box size. Under development!")
parser.add_argument("--cubic", action="store_true",               help="cube shaped box. Only works with --solvate")
parser.add_argument("--vol",          action="store_true",        help="do the lipid number estimation based on the volume occupied by the leaflet instead of APL. This might cause a great overestimation of the number of lipid molecules!")
parser.add_argument("--leaflet",    type=float, default=23.0,     help="set desired leaflet width. 23 by default.")
parser.add_argument("--lip_offset", type=float, default=1.0,      help="factor that multiplies the x/y sizes for the lipid membrane segment. Might improve packing and handling by AMBER")
parser.add_argument("--tailplane",  type=float,                   help="sets the position BELOW which the CH3 carbon atoms in the tail should be. By default defined in parameter file")
parser.add_argument("--headplane",  type=float,                   help="sets the position ABOVE which the PO4 phosphorus and N atoms in the polar head group should be.By default defined in parameter file")
parser.add_argument("--plot",         action="store_true",        help="makes a simple plot of loop number vs GENCAN optimization function value, and outputs the values to GENCAN.dat")
parser.add_argument("--traj",         action="store_true",        help="saves all intermediate steps into separate pdb files")
parser.add_argument("--notgridvol",   action="store_false",       help="skips grid building for volume estimation, and the calculation is done just by estimating density")
parser.add_argument("--notprotonate", action="store_false",       help="skips protonation by reduce")
parser.add_argument("--reducebuild",  action="store_true",        help="build step by reduce (protonation of His and side-chain flips). Use with care if parametrizing with the script!")
parser.add_argument("--keep",         action="store_false",       help="skips deleting temporary files")
parser.add_argument("--noprogress",   action="store_true",        help="avoids the printing of progress bar with time estimation in the final stage. Recommended if the job is piped into a file")
parser.add_argument("--overwrite",    action="store_true",        help="overwrite, even if files are present")
parser.add_argument("--nottrim",      action="store_false",       help="doesn't trim preexisting hydrogens in the structure")
parser.add_argument("--log",type=str,default="packmol-memgen.log",help="log file name where detailed information is to be written")
parser.add_argument("--output",     type=str,                     help="name of the PACKMOL generated PDB file")
parser.add_argument("--charmm",     action="store_true",          help="the output will be in CHARMM format instead of AMBER")
parser.add_argument("--translate", nargs=3, type=float, default=[0,0,0], help="pass a vector as  x y z  to translate the oriented pdb. Ex. ' 0 0 4 '")
parser.add_argument("--verbose",    action="store_true",          help="verbose mode")


inputs = parser.add_argument_group('Inputs')
inputs.add_argument("--pdb",           action="append",            help="PDB file(s) to embed. If many bilayers, it has to be specified once for each bilayer. 'None' can be specified and a bilayer without protein will be generated [ex. --pdb PDB1.pdb --pdb None --pdb PDB2.pdb (3 bilayers without protein in the middle)]. If no PDB is provided, the bilayer(s) will be membrane only (--distxy_fix has to be defined).")
inputs.add_argument("--solute",        action="append",            help="adds pdb as solute into the water. Concentration has to be specified")
inputs.add_argument("--solute_con",    action="append",            help="number of molecules/concentration to be used. Concentrations are specified in Molar by adding an 'M' as a suffix (Ex. 0.15M). If not added, a number of molecules is assumed.")
inputs.add_argument("--solute_charge", action="append",            help="absolute charge of the included solute. To be considered in the system neutralization")
inputs.add_argument("--solute_inmem",  action="store_true",        help="solute should be added to membrane fraction")

embedopt = parser.add_argument_group('MEMEMBED options')
embedopt.add_argument("--preoriented",  action="store_true",          help="use this flag if the protein has been previosuly oriented and you want to avoid running MEMEMBED (i.e. from OPM)")
embedopt.add_argument("--n_ter",        action="append",              help="'in' or 'out'. By default proteins are oriented with the n_ter oriented 'in' (or 'down'). relevant for multi layer system. If defined for one protein, it has to be defined for all of them, following previous order")
embedopt.add_argument("--barrel",       action="store_true",          help="use MEMEMBED in beta barrel mode")
embedopt.add_argument("--keepligs",     action="store_true",          help="MEMEMBED by default cleans the PDB. Use this flag to keep the ligands on the original PDB")
embedopt.add_argument("--mem_opt",type=str,default="3",choices=["0","1","2","3"],help="MEMEMBED optimization algorithm. 0 = Genetic algorithm, 1 = Grid, 2 = Direct, 3 = GA five times")
embedopt.add_argument("--memembed",type=str,                         help=argparse.SUPPRESS)

packmolopt = parser.add_argument_group('PACKMOL options')
packmolopt.add_argument("--nloop",       type=int,default=20,         help="number of nloops for GENCAN routine in PACKMOL. PACKMOL MEMGEN uses 20 by default; you might consider increasing the number to improve packing. Increasing the number of components requires more GENCAN loops.")
packmolopt.add_argument("--nloop_all",   type=int,default=100,        help="number of nloops for all-together packing. PACKMOL MEMGEN uses 100 by default.")
packmolopt.add_argument("--tolerance",   type=float,default=2.0,      help="tolerance for detecting clashes between molecules in PACKMOL (defined as radius1+radius2). PACKMOL uses 2.0 by default.")
packmolopt.add_argument("--prot_rad",   type=float,default=1.5,       help="radius considered for protein atoms to establish the tolerance for detecting clashes. PACKMOL MEMGEN uses 1.5 by default.")
packmolopt.add_argument("--writeout",                                 help="frequency for writing intermediate results. PACKMOL uses 10 by default.")
packmolopt.add_argument("--notrun",       action="store_false",       help="will not run PACKMOL, even if it's available")
packmolopt.add_argument("--random",       action="store_true",        help="turns PACKMOL random seed generator on. If a previous packing failed in the minimization problem, repacking with this feature on might solve the problem.")
packmolopt.add_argument("--packall",  action="store_true",            help="skips initial individual packing steps")
packmolopt.add_argument("--movebadrandom", action="store_true",       help="randomizes positions of badly placed molecules in initial guess")
packmolopt.add_argument("--packlog",type=str,default="packmol",       help="prefix for generated PACKMOL input and log files")
packmolopt.add_argument("--packmol",type=str,                         help=argparse.SUPPRESS)

saltopt = parser.add_argument_group('Salts and charges')
saltopt.add_argument("--salt",        action="store_true",         help="adds salt at a concentration of 0.15M by default. Salt is always added considering estimated charges for the system.")
saltopt.add_argument("--salt_c",default="K+",choices=["K+","Na+","Ca2+","Mg2+"], help="cation to add. (K+ by default)")
saltopt.add_argument("--saltcon", type=float, default=0.15,        help="modifies the default concentration for KCl. [M]")
saltopt.add_argument("--salt_override",action="store_true",        help="if the concentration of salt specified is less than the required to neutralize, will try to continue omitting the warning")

amberopt = parser.add_argument_group('AMBER options')
amberopt.add_argument("--minimize",     action="store_true",        help="performs minimization by using pmemd.")
amberopt.add_argument("--ligand_param",default=None,action="append",help="in case of parametrizing or minimizing the system with non-canonical molecules. Give frcmod and lib filenames separated by ':' [ex. --ligand_param FRCMOD:LIB].")
amberopt.add_argument("--gaff2",     action="store_true",           help="if ligand parameters are included, use GAFF2 during parametrization (GAFF is used by default)")
amberopt.add_argument("--leapline",default=None,action="append",    help="extra lines added to the leap parametrization process. Delimit the line with quotes. The system is called SYS into leap [ex. --leapline 'bond SYS.4.SG SYS.122.SG' to form SS bond between SG atoms of residues 4 and 122]")
amberopt.add_argument("--sander",        action="store_true",       help="use sander instead of pmemd.")
amberopt.add_argument("--parametrize",   action="store_true",       help="parametrizes the system by using tleap. Uses LIPID17 and ff14SB!")

compel = parser.add_argument_group('Computational electophysiology')
compel.add_argument("--double",       action="store_true",        help="asumes a stacked double bilayer system for CompEL. The composition in --lipids will be used for both bilayers flipping the leaflets")
compel.add_argument("--charge_imbalance", type=int, default=0,    help="sets a charge imbalance between compartments (in electron charge units). A positive imbalance implies an increase (decrease) in cations (anions) in the central compartment.")
compel.add_argument("--imbalance_ion", type=str, default="cat", choices=["cat","an"], help="sets if cations or anions are used to imbalance the system charges. ('cat' by default)")

logger = logging.getLogger()

def main(args):
    loghandle = logging.FileHandler(args.log,mode="a")
    loghandle.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:\n%(message)s',datefmt='%m/%d/%Y %I:%M:%S %p'))
    logger.addHandler(loghandle)
    streamer = logger.handlers[0]
    streamer.setLevel(logging.INFO)

    if args.verbose:
        streamer.setLevel(logging.DEBUG)
    else:
        print("--verbose for details of the packing process")
    logger.debug("Execution line:     "+" ".join(sys.argv))
    
    
    #Path to local Packmol installation (http://www.ime.unicamp.br/~martinez/packmol/home.shtml)
    rep = "data"
    lib = "lib"
    run = args.notrun
    packmol_inc = os.path.join(script_path, lib, "packmol", "packmol" + exe_suffix)
    if args.packmol is None:
        try:
            amberhome = os.environ['AMBERHOME']
            packmol_amb = os.path.join(amberhome, "bin", "packmol" + exe_suffix)
        except:
            packmol_amb = ""
    
        if os.path.exists(packmol_inc):
            packmol = packmol_inc
        elif os.path.exists(packmol_amb):
            packmol = packmol_amb
        else:
            packmol = ""
    else:
        packmol = args.packmol
    
    if (packmol != None or packmol != "") and not os.path.exists(packmol):
        miss = "Packmol path defined but not found on "+packmol
        logger.info("\n"+len(miss)*"#"+"\n"+miss+"\n"+len(miss)*"#"+"\n")
        run = False
    elif packmol == None or packmol == "":
        miss = "Packmol path not defined. Execution will only create the packmol input script."
        logger.info("\n"+len(miss)*"#"+"\n"+miss+"\n"+len(miss)*"#"+"\n")
        run = False
    
    
    ############## PARSE PARAMS FROM REP #########
    
    def is_number(num):
        try:
            float(num)
            return True
        except:
            return False
    
    parms = open(os.path.join(script_path, rep, "memgen.parm"), "r")
    pdbtar = tarfile.open(os.path.join(script_path, rep,"pdbs.tar.gz"),"r:gz")
    parmlines = parms.readlines()
    parms.close()
    parameters = {}
    ext_par = False
    for line in parmlines:
        if line[0] == "#":
            if line.startswith("# PIPS"):
                ext_par = True
            continue
        else:
            values = line.split()
            parameters[values[0]]  = {}
            parameters[values[0]].update({"p_atm":values[1],"t_atm":values[2],"APL":values[3],"V":values[4],"charge":values[5],"h_bound":values[6],"t_bound":values[7],"C":values[8],"VH":values[9],"charmm":values[10],"name":values[11],"ext":ext_par})
    
    if args.available_lipids or args.available_lipids_all:
        print("{:<9}{:>6}{:>95}{:>15}".format("Lipid","Charge","Full-name","Comment"))
        for key in sorted(parameters):
            if not args.available_lipids_all and parameters[key]["ext"]:
                continue
            elif is_number(key[0]):
                print("{:<9}{:>6}{:>95} ***Lysophospholipid. BETA!".format(key,parameters[key]["charge"],parameters[key]["name"]))
            elif key[-2:] == "SM":
                print("{:<9}{:>6}{:>95} ***Not available in AMBER".format(key,parameters[key]["charge"],parameters[key]["name"]))
            else:
                print("{:<9}{:>6}{:>95}".format(key,parameters[key]["charge"],parameters[key]["name"]))
        exit()
    
    
    ############ ARGPARSE ARGUMENTS ###################

    if not args.solvate and args.cubic:
        logger.warning("--cubic only available with --solvate. Turned off!")
        args.cubic = False
    
    if args.solvate:
        args.preoriented = True
        z_cen = True
        pdb_prefix = "solvated"
        args.dist_wat = args.dist
    else:
        pdb_prefix = "bilayer"
        z_cen = False

    onlymembrane = False
    if args.pdb is None or args.pdb.count("None") == len(args.pdb):
        onlymembrane = True
    
    if args.minimize:
        args.parametrize = True
    
    outfile = args.output
    if outfile is not None:
        if not outfile.endswith(".pdb"):
            outfile = outfile+".pdb"
    if not args.output and args.pdb is None:
        outfile = pdb_prefix+"_only.pdb"
    elif not args.output:
        outfile = pdb_prefix+"_"+"".join([os.path.basename(pdb).replace(".pdb","") for pdb in args.pdb])+".pdb"
        
    leaflet_z = args.leaflet # leaflet thickness
    lip_offset = args.lip_offset
    bound_tail = args.tailplane # plane that defines boundary to last carbon in aliphatic chain
    bound_head = args.headplane # plane that defines boundary to polar head
    lipids = args.lipids # lipids to be used
    extended = False
    if lipids is not None:
        if any([is_number(l[0]) for lipid in lipids for l in lipid.split(":")]) or any(["PI" in l for lipid in lipids for l in lipid.split(":")]):
            extended = True
    ratio = args.ratio # mixture ratio
    saltcon = args.saltcon # salt concentration in M
    if not args.salt:
        if "--saltcon" in sys.argv and (args.distxy_fix is not None or args.pdb is not None):
            logger.error("You specified a salt concentration, but not the salt flag. Only neutralizing ions will be added")
        saltcon = 0
        if args.charge_imbalance != 0:
            logger.error("You specified a charge imbalance, but not the salt flag. No charge imbalance will be applied")
    override_salt = args.salt_override
    #Check for cation
    ion_dict = {"K+":("POT",1),"Na+":("SOD",1),"Ca2+":("CAL",2),"Cl-":("CLA",-1),"Mg2+":("MG",2)}
    if args.salt_c not in ion_dict:
        logger.error("The specified cation option is no available at the moment")
        exit()
    else:
        cation = ion_dict[args.salt_c][0]
    distance = args.dist # distance from the protein to the box boundaries to XY
    distance_wat = args.dist_wat # minimum distance from the surface of the membrane to the box boundary on Z

    asym = False
    if args.distxy_fix is not None and args.dims == [0,0,0]:
        args.dims = [args.distxy_fix,args.distxy_fix,0]
    elif args.dims == [0,0,0]:
        args.dims = None 
    else:
        asym = True
    
    if (args.distxy_fix is None and args.dims is None) and onlymembrane:
        logger.error("No PDB file given or fixed XY dimensions specified. Use -h for help.")
        exit()
    nloop = args.nloop
    nloop_all = args.nloop_all
    if args.writeout is None:
        if int(nloop) < 10 or int(nloop_all) < 10:
            logger.error("nloop and nloop_all have to be bigger than the writeout frequency (every 10 loops by default). You can modify this with --writeout")
            exit()
    else:
        if int(nloop) < int(args.writeout) or int(nloop_all) < int(args.writeout):
            logger.error("nloop and nloop_all have to be bigger than the writeout frequency! Modify the used values.")
            exit()
    protonate = args.notprotonate
    Trim = args.nottrim
    grid_calc = args.notgridvol
    delete = args.keep
    onlydots = args.noprogress

    # JSwails suggestion//Check if in a tty. Turn off progress bar if that's the case. 
    if not os.isatty(sys.stdin.fileno()):
        onlydots = True
    
    # Make a list with created files for later deletion
    created        = []
    created_notrun = []
    
    ###############################################
    ###############################################
    ###############################################
    
    ############ SEARCH AMBER, LIBS, PDB, MEMEMBED ###################
    
    try:
        amberhome = os.environ['AMBERHOME']
        reduce = os.path.join(amberhome, "bin", "reduce" + exe_suffix)
        if not os.path.exists(reduce):
            logger.warning("AMBER path found, but reduce executable is not available")
    except:
        amberhome = None
        reduce = None
        logger.debug("AMBER environment variables not found. Protonation will not be available!")
    
    if not os.path.exists(os.path.join(script_path, rep)):
        logger.critical("The data folder for using the script is missing. Check the path for the script!")
        exit()
    
    if args.pdb is not None:
        if args.pdb[0] == "None" and not onlymembrane:
            logger.error("Please specify first the protein PDB file!")
            exit()
        for pdb in args.pdb:
            if not os.path.exists(pdb) and pdb != "None":
                logger.error("Either the options were wrongly used or the file "+pdb+" doesn't exist!")
                exit()
    
    memembed_inc = os.path.join(script_path, lib, "memembed", "bin", "memembed" + exe_suffix)
    if args.memembed is None:
        try:
            amberhome = os.environ['AMBERHOME']
            memembed_amb = os.path.join(amberhome, "bin", "memembed" + exe_suffix)
        except:
            memembed_amb = ""

        if os.path.exists(memembed_inc):
            memembed = memembed_inc
        elif os.path.exists(memembed_amb):
            memembed = memembed_amb
        else:
            memembed = ""
    else:
        memembed = args.memembed

    if not os.path.exists(memembed):
        miss = "MEMEMBED not found. The system will be built assuming it was preoriented"
        logger.warning(logger.info("\n"+len(miss)*"#"+"\n"+miss+"\n"+len(miss)*"#"+"\n"))
        memembed = False
    
    if not os.path.exists(os.path.join(script_path, lib, "pdbremix")):
        logger.warning("PDBREMIX lib not available. Volume estimation will be done based on estimated density")
    else:
        grid_avail = True
    
    ############# SET CONSTANTS AND CHECK LIPIDS ################
    
    VCH           = 21.65 # A^3
    VCH2          = 27.03 # A^3
    avogadro      = 6.02214086*10**23
    water_density = 1 # g/mol
    water_con     = water_density*avogadro/(18*10**24) #Water molecules per A3
    
    if lipids is None:
        lipids = ["DOPC"]
    if args.double and len(lipids) == 1:
        lipids = lipids+["//".join(reversed(lipids[0].split("//")))]
    elif args.double and len(lipids) != 1:
        logger.error("--double requires a single definition of lipid composition")
        exit()
    if ratio is None:
        ratio  = ["1"]*len(lipids)
    if args.double and len(lipids) == 2 and len(ratio) == 1:
        ratio = ratio+["//".join(reversed(ratio[0].split("//")))]
    
    if len(lipids) != len(ratio):
        logger.error("Number of defined bilayer lipids and ratios doesn't fit! Check your input")
        exit()
    
    if args.n_ter is None and args.double:
        args.n_ter = ["in","out"]
    elif args.n_ter is None:
        args.n_ter = ["in"]*len(lipids)
        
    elif len(args.n_ter) != len(args.pdb):
        logger.error("Number of specified orientations and bilayers doesn't fit! Check your input")
        exit()
    else:
        for n, pdb in enumerate(args.pdb):
            if pdb != "None" and not (args.n_ter[n] == "in" or args.n_ter[n] == "out"):
                logger.error("The orientation has to be 'in' or 'out' unless pdb is 'None'")
                exit()
				
    if args.solute_charge is None and args.solute is not None:
        args.solute_charge = [0]*len(args.solute)
    
    elif args.solute_charge is not None and args.solute is not None:    
        if len(args.solute_charge) != len(args.solute):
            logger.error("Number of specified solute charges and solutes doesn't fit! Check your input")
            exit()
        else:
            for n, solute in enumerate(args.solute):
                try:
                    args.solute_charge[n] = int(args.solute_charge[n])
                except:
                    logger.error("Solute charges have to be integers")
                    exit()    
	
    composition = {}
    for bilayer in range(len(lipids)):
        composition[bilayer] = {}
        if "//" in lipids[bilayer]:
            if len(lipids[bilayer].split("//")) != 2 or len(ratio[bilayer].split("//")) != 2:
                logger.error("If different leaflet compositions used, ratios have to be specified explicitly, and both must be separated by one '//' only!")
                exit()
            for leaflet in range(2):
                if len(lipids[bilayer].split("//")[leaflet].split(":")) != len(ratio[bilayer].split("//")[leaflet].split(":")):
                    logger.error("Amount of lipid types and ratios doesn't fit! Check your input")
                    exit()
                composition[bilayer][leaflet]={}
                ratio_total = sum([float(x) for x in ratio[bilayer].split("//")[leaflet].split(":")])
                for n, lipid in enumerate(lipids[bilayer].split("//")[leaflet].split(":")):
                    if lipid == "PSM" or lipid == "SSM":
                        logger.warning("LIPID17 doesn't currently support sphingomyelin! AMBER parametrization will be disabled.")
                        args.minimize    = False
                        args.parametrize = False   
                    composition[bilayer][leaflet][lipid] = float(ratio[bilayer].split("//")[leaflet].split(":")[n])/ratio_total
        else:
            if len(lipids[bilayer].split(":")) != len(ratio[bilayer].split(":")):
                logger.error("Amount of lipid types and ratios doesn't fit! Check your input")
                exit()
            composition[bilayer][0]={} ; composition[bilayer][1]={}
            ratio_total = sum([float(x) for x in ratio[bilayer].split(":")])
            for n, lipid in enumerate(lipids[bilayer].split(":")):
                if lipid == "PSM" or lipid == "SSM":
                    logger.warning("LIPID17 doesn't currently support sphingomyelin! AMBER parametrization will be disabled.")
                    args.minimize    = False
                    args.parametrize = False
                composition[bilayer][0][lipid] = composition[bilayer][1][lipid] = float(ratio[bilayer].split(":")[n])/ratio_total
    
    for key in [x for leaflet in composition[bilayer] for bilayer in composition for x in composition[bilayer][leaflet]]:
        if key not in parameters:
            logger.critical("Parameters missing for "+key+". Please check the file memgen.parm")
            exit()
        if args.charmm and parameters[key]["charmm"] == "N":
            logger.critical("Lipid "+key+" not available in CHARMM format.")
            exit()
        if not os.path.exists(os.path.join(script_path, rep, "pdbs", key +".pdb")) and key+".pdb" not in pdbtar.getnames():
            logger.critical("PDB file for "+key+" not found!")
            exit()
            
    if not onlymembrane:
        if len(composition) != len(args.pdb):
            if args.double and len(composition) == 2 and len(args.pdb) == 1:
                logger.debug("Using the pdb file for both bilayers")
                args.pdb = args.pdb*2
            else:
                logger.critical("The number of provided PDB files doesnt fit with the number of bilayers. Pass --pdb \"None\" if you want an empty bilayer.")
                exit()
    
    if args.solute is not None:
        if args.solute_con is not None:
            if len(args.solute) != len(args.solute_con):
                logger.error("Number of solutes and concentrations/number of molecules is not the same! Please provide a concentration for each solute in the respective order.")
                exit()
        else:
            logger.error("Concentrations/number of molecules have to be provided.")
            exit()
            
    
    if args.ligand_param is not None:
        for param in args.ligand_param:
            files = param.split(":")
            if not os.path.exists(files[0]) or not os.path.exists(files[1]):
                logger.error("Specified parameter files do not exist!")
                logger.debug("Files: "+" ".join(files))
                exit()
    
    ############## SET OF USED FUNCTIONS  #####################
    
    def estimated_density(MW):
        density = 1.41 + 0.145*math.exp(float(-MW)/13000)  #Protein Sci. 2004 Oct; 13(10):2825-2828
        return density
    
    def memembed_align(pdb,keepligs=False,verbose=False,overwrite=False,barrel=False,n_ter="in",opt="3"):
        output = pdb[:-4]+n_ter+"_EMBED.pdb"
        if os.path.exists(output) and not overwrite:
            return output
        if not barrel:
            os.system(memembed+" -s "+opt+" -n "+n_ter+" -o "+output+" "+pdb+" > "+output.replace("_EMBED.pdb","_memembed.log"))
        else:
            os.system(memembed+" -s "+opt+" -b -n "+n_ter+" -o "+output+" "+pdb+" > "+output.replace("_EMBED.pdb","_memembed.log"))
        if keepligs:
            if verbose:
                logger.info("Superimposing to keep ligands")
            rmsd_of_pdbs(pdb,output,transform_pdb1=output.replace("_EMBED.pdb","_EMBED_ligs.pdb"), standard=True)
            return output.replace("_EMBED.pdb","_EMBED_ligs.pdb")
        return output
    
    def pdbvol(pdb,spacing=0.5,overwrite=False):
        output = pdb[:-4]+".grid.pdb"
        if os.path.exists(output) and not overwrite:
            filelength = len(open(output,"r").readlines())
            vol        = filelength*spacing**3
            return (output, vol)
        else:
            filelines = open(pdb,"r").readlines()
            temp_pdb = []
            for line in filelines:
                if (line[0:4] == "ATOM" or line[0:6] == "HETATM") and  line[17:20].strip() != "DUM":
                    temp_pdb.append(line)
            temp = open("temp.pdb","w").writelines(temp_pdb)
            atoms = pdbatoms.read_pdb("temp.pdb")
            pdbatoms.add_radii(atoms)
            vol = volume(atoms, spacing, output, verbose=False)
            os.remove("temp.pdb")
            return (output, vol)
    
    if args.solute is not None:
        for i, sol in enumerate(args.solute):
            if not os.path.exists(args.solute[i]):
                logger.error(args.solute[i]+" not found!")
                exit()
            logger.info("Extra solute PDB         = %-9s" % (args.solute[i]))
            logger.info("Solute to be added       = %-9s" % (args.solute_con[i]))
    
    

    if os.path.exists(outfile) and not args.overwrite:
        logger.info("Packed PDB "+outfile+" found. Skipping PACKMOL")
    else:
        ############################### SCRIPT HEADER ################################
        
        content_header = content_prot = content_lipid = content_water = content_ion = content_solute = ""
        content_header += "tolerance "+str(args.tolerance)+"\n"
        content_header += "filetype pdb\n"
        content_header += "output "+outfile+"\n\n"
        if args.writeout is not None:
            content_header += "writeout "+args.writeout+"\n"
        if args.traj:
            if args.writeout is None:
                content_header += "writeout 1\n"
            content_header += "writebad\n\n"
        
        if args.random:
            content_header += "seed -1\n"
        if args.packall:
            content_header += "packall\n"
        if not args.charmm:
             content_header += "add_amber_ter\n"
        if args.movebadrandom:
            content_header += "movebadrandom\n" 
        
        content_header += "nloop "+str(nloop_all)+"\n\n"
#        content_header += "nloop_all "+str(nloop_all)+"\n\n"
   
        pond_lip_vol_dict      = {}
        pond_lip_apl_dict      = {}
        lipnum_dict            = {}
        lipnum_area_dict       = {}
    
        X_min   = X_max = X_len = Y_min = Y_max = Y_len = 0
        Z_dim   = []
        memvol  = []
        charges = []
        if not onlymembrane:
            chain_nr = 0
            chain_index = list(string.ascii_uppercase)
            for n,pdb in enumerate(args.pdb):
                if pdb != "None":    
                    if not args.verbose:
                        logger.info("Preprocessing "+pdb+". This might take a minute.")
                    if protonate and reduce:
                        logger.debug("Using 'reduce' to protonate...")
                        if Trim:
                            logger.debug("Trimming preexisting protons...")
                            pdb =  reduce_trim(pdb,overwrite=args.overwrite)
                            if pdb == "error":
                                logger.error("reduce returned an empty file! Check the used pdb files and reduce_Trim_warn.log")
                                exit()
                            created.append(pdb)
                            created.append("reduce_Trim_warn.log")
                        logger.debug("Adding protons...")
                        pdb =  reduce_protonate(pdb,overwrite=args.overwrite,build=args.reducebuild)
                        if pdb == "error":
                                logger.error("reduce returned an empty file! Check the used pdb files and reduce_warn.log")
                                exit()
                        created.append(pdb)
                        created.append("reduce_warn.log")
    
                    if not args.preoriented and memembed:
                        logger.debug("Aligning the protein using MEMEMBED...")
                        created.append(pdb+"_memembed.log")
                        pdb = memembed_align(pdb,keepligs=args.keepligs,verbose=args.verbose,overwrite=args.overwrite,barrel=args.barrel,n_ter=args.n_ter[n],opt=args.mem_opt)
                        created.append(pdb)
                        
                    if grid_calc:
                        logger.debug("Estimating the volume by building a grid (PDBREMIX)...")
                        grid = pdbvol(pdb)
                        created.append(grid[0])
                    else:
                        grid = (None,None)
    
                    ############## FAST VALUE ESTIMATION FROM PDB #####################
                    minmax, max_rad, charge_prot, vol, memvol_up, memvol_down, solvol_up, solvol_down, density, mass, chains = measure_parms(pdb,leaflet_z,grid[0],move=True, move_vec=args.translate, xy_cen=True, z_cen=z_cen, outpdb="PROT"+str(n)+".pdb",chain=chain_index[chain_nr],renumber=True)
                    chain_nr += chains
                    charges.append(charge_prot)
                    if args.double:
                        minmax, max_rad, charge_prot, vol, memvol_up, memvol_down, solvol_up, solvol_down, density, mass, chains = measure_parms(pdb,leaflet_z,grid[0],move=True, move_vec=args.translate, xy_cen=True, z_cen=z_cen, outpdb="PROT"+str(n+1)+".pdb",chain=chain_index[chain_nr],renumber=True)
                        chain_nr += chains
                        charges.append(charge_prot)
                else:
                    if not args.verbose:
                        print("Building membrane without protein!")
                    if args.dims is not None:
                        minmax      = [-args.dims[0]/2,-args.dims[1]/2,-leaflet_z,args.dims[0]/2,args.dims[1]/2,leaflet_z]
                    #TEST!!!
                    else:
                        minmax      = minmax[0:2]+[-leaflet_z]+minmax[3:5]+[leaflet_z]
                    charge_prot      = 0
                    charges.append(charge_prot)
                    vol              = 0
                    memvol_up        = 0
                    memvol_down      = 0
                    solvol_up        = 0
                    solvol_down      = 0
                    density          = 0
                    mass             = 0
    
                memvol.append((memvol_down,memvol_up))
                if args.double:
                    memvol.append((memvol_down,memvol_up))
   
                com = [(minmax[0]+minmax[3])/2,(minmax[1]+minmax[4])/2,(minmax[2]+minmax[5])/2]

                if args.dims is not None:
                    pdbx_min     = com[0]-args.dims[0]/2
                    pdbx_max     = com[0]+args.dims[0]/2
                    pdby_min     = com[1]-args.dims[1]/2 
                    pdby_max     = com[1]+args.dims[1]/2
                    pdbz_min     = com[2]-args.dims[2]/2
                    pdbz_max     = com[2]+args.dims[2]/2
                                                       
                else: 
                    pdbx_min     = minmax[0]
                    pdbx_max     = minmax[3]
                    pdby_min     = minmax[1]
                    pdby_max     = minmax[4]
                    pdbz_min     = minmax[2]
                    pdbz_max     = minmax[5]

                pdbx_len     = pdbx_max-pdbx_min
                pdby_len     = pdby_max-pdby_min
                pdbz_len     = pdbz_max-pdbz_min

                if not asym:
                    if args.dims is None:
        #                max_side_len = math.sqrt(pdbx_len**2+pdby_len**2)+2*distance
                        max_side_len = 2*max_rad+2*distance
                    else:
                        max_side_len = max(args.dims[:2])
                    if args.cubic:
                        max_side_len = max(max_side_len,pdbz_len+2*distance)
                    diff_x       = max_side_len - pdbx_len
                    diff_y       = max_side_len - pdby_len
                    diff_z       = max_side_len - pdbz_len
                else:
                    diff_x = diff_y = diff_z = 0
                x_min        = pdbx_min-diff_x/2
                x_max        = pdbx_max+diff_x/2
                y_min        = pdby_min-diff_y/2
                y_max        = pdby_max+diff_y/2
                z_min        = pdbz_min-diff_z/2
                z_max        = pdbz_max+diff_z/2
                x_len        = x_max-x_min
                y_len        = y_max-y_min
                z_len        = z_max-z_min
   
                if not args.cubic and not asym:
                    z_min = pdbz_min-distance_wat
                    if z_min > -(leaflet_z+distance_wat):
                        z_min = -(leaflet_z+distance_wat)
                    z_max = pdbz_max+distance_wat
                    if z_max < (leaflet_z+distance_wat):
                        z_max =  (leaflet_z+distance_wat)
                    z_len = z_max-z_min
    
                if x_min < -1000 or x_max > 1000 or y_min < -1000 or y_max > 1000 or z_min < -1000 or z_max > 1000:
                    logger.warning("The size of the system is bigger than the default accepted values for PACKMOL. The flag sidemax will be added.")
                    content_header += "sidemax "+str(max(abs(max(x_max,y_max,z_max)),abs(min(x_min,y_min,z_min))))+"\n"
    
                prot_data2= """
    Estimated values for input protein:
    
    Input PDB                = %-9s
    Charge                   = %-9s 
    Mass                     = %-9s    Da
    Density                  = %-9s    Da/A^3 
    Estimated volume         = %-9s    A^3
        in upper leaflet     = %-9s    A^3
        in lower leaflet     = %-9s    A^3
        in upper water box   = %-9s    A^3
        in lower water box   = %-9s    A^3
                """
                logger.debug(prot_data2 % ( pdb, charge_prot, mass, round(density,2), round(vol,2), round(memvol_up,2), round(memvol_down,2), round(solvol_up), round(solvol_down,2)))
    
                
                if x_len > X_len:
                    X_min = x_min; X_max = x_max; X_len = x_len
                if y_len > Y_len:
                    Y_min = y_min; Y_max = y_max; Y_len = y_len
                Z_dim.append((z_min,z_max,z_len))
                if args.double:
                    Z_dim.append((z_min,z_max,z_len))
                    break
        else:
            if args.dims is not None:
                minmax      = [-args.dims[0]/2,-args.dims[1]/2,-args.dims[2]/2,args.dims[0]/2,args.dims[1]/2,args.dims[2]/2]
                z_min = minmax[2]
                z_max = minmax[5]
            else:
                z_min = minmax[2]-distance_wat
                z_max = minmax[5]+distance_wat

            charge_prot      = 0
            charges          = [charge_prot]*len(composition)
            vol              = 0
            memvol_up        = 0
            memvol_down      = 0
            solvol_up        = 0
            solvol_down      = 0
            density          = 0
            mass             = 0
    
            memvol = [(memvol_down,memvol_up)]*len(composition)
    
            pdbx_len     = minmax[3]-minmax[0]
            pdby_len     = minmax[4]-minmax[1]

            if not asym:
                max_side_len = max(pdbx_len,pdby_len)
                diff_x       = max_side_len - pdbx_len
                diff_y       = max_side_len - pdby_len
            else:
                diff_x = diff_y = 0
            x_min        = minmax[0]-diff_x/2
            x_max        = minmax[3]+diff_x/2
            y_min        = minmax[1]-diff_y/2
            y_max        = minmax[4]+diff_y/2
            x_len        = x_max-x_min
            y_len        = y_max-y_min
    
            if z_min > -(leaflet_z+distance_wat):
                z_min = -(leaflet_z+distance_wat)
            if z_max < (leaflet_z+distance_wat):
                z_max =  (leaflet_z+distance_wat)
            z_len = z_max-z_min
    
            if x_min < -1000 or x_max > 1000 or y_min < -1000 or y_max > 1000 or z_min < -1000 or z_max > 1000:
                logger.warning("The size of the system is bigger than the default accepted values for PACKMOL. The flag sidemax will be added.")
                content_header += "sidemax "+str(max(abs(max(x_max,y_max,z_max)),abs(min(x_min,y_min,z_min))))+"\n"
    
            if x_len > X_len:
                X_min = x_min; X_max = x_max; X_len = x_len
            if y_len > Y_len:
                Y_min = y_min; Y_max = y_max; Y_len = y_len
            Z_dim = [(z_min,z_max,z_len)]*len(composition)
        
        prot_data1 = """ 
    Information for packing:
    
    Input PDB(s)                     = %-9s
    Output PDB                       = %-9s
    Packmol output and log prefix    = %-9s
    Lipids                           = %-9s
    Lipid ratio                      = %-9s
    Salt concentration (M)           = %-9s
    Distance to boundaries           = %-9s
    Minimum water distance           = %-9s
    Packmol loops                    = %-9s
    Packmol loops for All-together   = %-9s"""

        if not args.solvate:    
            logger.info(prot_data1 % ( args.pdb, outfile, args.packlog, lipids, ratio, saltcon, distance, distance_wat, nloop, nloop_all))
        else:
            logger.info(prot_data1 % ( args.pdb, outfile, args.packlog, "-", "-", saltcon, distance, distance_wat, nloop, nloop_all))
    
    
        box_info = """
    Box information:
    x_min                    = %-9s    
    x_max                    = %-9s   
    x_len                    = %-9s
    
    y_min                    = %-9s    
    y_max                    = %-9s    
    y_len                    = %-9s
    
    z_min                    = %-9s    
    z_max                    = %-9s
    z_len                    = %-9s
    """
        logger.debug(box_info % ( X_min, X_max, X_len, Y_min, Y_max, Y_len, Z_dim[0][0], Z_dim[0][0]+sum([zdim[2] for zdim in Z_dim]), sum([zdim[2] for zdim in Z_dim]) ))
    
        z_offset = 0
    
        for n, bilayer in enumerate(composition):
            if bilayer > 0:
                z_offset = z_offset+Z_dim[bilayer-1][1]-Z_dim[bilayer][0]
           
            ################################## PROTEIN ###################################
     
            if not onlymembrane:
                if args.pdb[bilayer] != "None":
                    created_notrun.append("PROT"+str(bilayer)+".pdb")
                    content_prot += "structure PROT"+str(bilayer)+".pdb\n"
                    content_prot += "  number 1\n"
                    content_prot += "  fixed 0. 0. "+str(z_offset)+" 0. 0. 0.\n"
                    content_prot += "  radius "+str(args.prot_rad)+"\n"
                    content_prot += "end structure\n\n"
            
            ################################ LIPIDS ######################################
           
            lipid_vol_up           = ((X_len+2*lip_offset)*(Y_len+2*lip_offset)*leaflet_z)-memvol_up
            lipid_vol_down         = ((X_len+2*lip_offset)*(Y_len+2*lip_offset)*leaflet_z)-memvol_down
            lipid_vol              = (lipid_vol_down,lipid_vol_up)
            lipid_area             = X_len*Y_len
            pond_lip_vol_dict[bilayer]      = {}
            pond_lip_apl_dict[bilayer]      = {}
            lipnum_dict[bilayer]            = {}
            lipnum_area_dict[bilayer]       = {}
            for leaflet in composition[bilayer]:
                pond_lip_vol = 0
                pond_lip_apl = 0
                for lipid in composition[bilayer][leaflet]:
                    if parameters[lipid]["V"] == "XXXX":
                        if parameters[lipid]["C"] == "X":
                            logger.critical("Volume not specified and can not be estimated! Check the parameter file")
                            exit()
                        nCH3,nCH2,nCH = list(map(int,parameters[lipid]["C"].split(":")))
                        parameters[lipid]["V"] = str(int(round(float(parameters[lipid]["VH"])+nCH3*2*VCH2+nCH2*VCH2+nCH*VCH)))         #doi:10.1016/j.bbamem.2005.07.006
                        logger.debug("Experimental value for "+lipid+" volume not available in parm file. Using estimated "+parameters[lipid]["V"]+" A^3 instead...")
                    if parameters[lipid]["APL"] == "XX":
#                        parameters[lipid]["APL"] = "75"
                        try:
                            parameters[lipid]["APL"] = max([float(parameters[lip]["APL"]) for lip in parameters if (is_number(parameters[lip]["APL"]) and lip.endswith(lipid[-2:]))])
                            logger.debug("Taking maximal APL of lipids with headgroup "+lipid[-2:])
                        except:
                            parameters[lipid]["APL"] = "75"
                            logger.debug("No other lipid with same headgroup has APL. Setting APL of 75 to "+lipid[-2:])
                        logger.debug("Value for "+lipid+" area per lipid not available in parm file. Using "+str(parameters[lipid]["APL"])+" A^2 instead...")
                    pond_lip_vol += composition[bilayer][leaflet][lipid]*int(parameters[lipid]["V"])
                    pond_lip_apl += composition[bilayer][leaflet][lipid]*int(parameters[lipid]["APL"])
                lipnum = lipid_vol[leaflet]/pond_lip_vol
                lipnum_area = lipid_area/pond_lip_apl-(memvol[bilayer][leaflet]/pond_lip_vol)
                pond_lip_vol_dict[bilayer][leaflet]      = pond_lip_vol
                pond_lip_apl_dict[bilayer][leaflet]      = pond_lip_apl
                lipnum_dict[bilayer][leaflet]            = lipnum
                lipnum_area_dict[bilayer][leaflet]       = lipnum_area
                        
            charge_lip = charge_solute = 0
            if not args.vol:
                lipnum_dict = lipnum_area_dict

            ################################ SOLUTE ######################################

            if not args.solvate:
                if args.solute is not None and args.solute_inmem:
                    for i,sol in enumerate(args.solute):
                            logger.info("Adding "+args.solute_con[i]+" "+args.solute[i]+" to the lipid volume")
                            grid_file, solute_vol      = pdbvol(args.solute[i])
                            created.append(grid_file)
                            if args.solute_con[i].endswith("M") and is_number(args.solute_con[i][:-1]):
                                solute_num      = int(float(args.solute_con[i][:-1])*((sum(lipid_vol)*avogadro/(1*10**27))))
                            elif args.solute_con[i].endswith("%") and is_number(args.solute_con[i][:-1]):
                                solute_num      = int(float(args.solute_con[i][:-1])/100*((sum(lipid_vol)*avogadro/(1*10**27))))
                            elif is_number(args.solute_con[i]):
                                try:
                                    int(args.solute_con[i])
                                except:
                                    logger.error("A number less than 1 is specified. If a concentration was intended, add M/% as a suffix!")
                                    exit()
                                solute_num      = int(args.solute_con[i])
                            else:
                                logger.error("The format used to specify the number of molecules is not correct! It has to be a number or a number with an M/% suffix.")
                                exit()
                            if solute_num < 1:
                                logger.error("The solute concentration is too low for the calculated box size. Try increasing the concentration.")
                                exit()
                            solute_vol_tot  = solute_vol*solute_num
                            print(args.solute_charge[i],solute_num)
                            charge_solute += args.solute_charge[i]*solute_num
                            for leaflet in lipnum_dict[bilayer]:
                                lipnum_dict[bilayer][leaflet] -= int((solute_vol_tot)/(2*pond_lip_vol_dict[bilayer][leaflet]))

                            content_solute += "structure "+args.solute[i]+"\n"
                            content_solute += "  number "+str(int(solute_num))+"\n"
                            content_solute += "  inside box "+str(round(X_min*lip_offset,2))+" "+str(round(Y_min*lip_offset,2))+" "+str(z_offset-leaflet_z)+" "+str(round(X_max*lip_offset,2))+" "+str(round(Y_max*lip_offset,2))+" "+str(z_offset+leaflet_z)+"\n"
                            content_solute += "end structure\n\n"
                elif (args.solute is not None or args.solute_con is not None) and args.solute_inmem:
                    logger.error("The solute parameters are incomplete. Please include both a solute pdb and a concentration.")
                    exit()
           
            
            if not args.solvate:
                for leaflet in composition[bilayer]:
                    for lipid in sorted(composition[bilayer][leaflet], reverse=True, key=lambda x: composition[bilayer][leaflet][x]):
                        if args.headplane is None:
                            bound_head=float(parameters[lipid]["h_bound"])
                            if bound_head >= leaflet_z:
                                logger.error("The boundary for "+lipid+" head group is out of the space delimited for the membrane by the membrane width "+leaflet_z+". Please consider increasing the value!")
                                exit()
                        if args.tailplane is None:
                            bound_tail=float(parameters[lipid]["t_bound"])
                            if bound_tail <=0:
                                logger.error("The boundary for "+lipid+" tail is out of the space delimited for the membrane by the membrane center at the z axis origin (it must be a positive value greater than 0). Please consider increasing the value!")
                                exit()
                        if not os.path.isfile("./"+lipid+".pdb"):
                            try:
                                shutil.copy(script_path+rep+"/pdbs/"+lipid+".pdb", "./")
                            except:
                                pdbtar.extract(lipid+".pdb")
                            created_notrun.append(lipid+".pdb")

                        content_lipid += "structure "+lipid+".pdb\n"
                        content_lipid += "  nloop "+str(nloop)+"\n"
                        content_lipid += "  number "+str(int(round(composition[bilayer][leaflet][lipid]*lipnum_dict[bilayer][leaflet])))+"\n"
                        content_lipid += "  inside box "+str(round(X_min*lip_offset,2))+" "+str(round(Y_min*lip_offset,2))+" "+str(z_offset-leaflet_z+leaflet_z*leaflet)+" "+str(round(X_max*lip_offset,2))+" "+str(round(Y_max*lip_offset,2))+" "+str(z_offset+leaflet_z*leaflet)+"\n"
                        content_lipid += "  atoms "+" ".join([p_atm for p_atm in parameters[lipid]["p_atm"].split(",")])+"\n"
                        if leaflet == 0:
                            content_lipid += "    below plane 0. 0. 1. "+str(-bound_head+z_offset)+"\n"
                        else:
                            content_lipid += "    over plane 0. 0. 1. "+str(bound_head+z_offset)+"\n"
                        content_lipid += "  end atoms\n"
                        content_lipid += "  atoms "+" ".join([t_atm for t_atm in parameters[lipid]["t_atm"].split(",")])+"\n"
                        if leaflet == 0:
                            content_lipid += "    over plane 0. 0. 1. "+str(-bound_tail+z_offset)+"\n"
                        else:
                            content_lipid += "    below plane 0. 0. 1. "+str(bound_tail+z_offset)+"\n"
                        content_lipid += "  end atoms\n"
                        content_lipid += "end structure\n\n"

            ######################## WATER && SOLUTE #####################################

            try:            
                shutil.copy(os.path.join(script_path, rep, "pdbs", "TIP3P.pdb"), "./")
            except:
                pdbtar.extract("TIP3P.pdb")
            created_notrun.append("TIP3P.pdb")

            water_vol_up   = (X_len*Y_len*(abs(Z_dim[bilayer][1])-leaflet_z))-solvol_up
            water_vol_down = (X_len*Y_len*(abs(Z_dim[bilayer][0])-leaflet_z))-solvol_down
            if args.solvate:
                water_vol_up   = water_vol_up+lipid_vol_up
                water_vol_down = water_vol_down+lipid_vol_down

            watnum_up = int(water_vol_up*water_con)
            watnum_down = int(water_vol_down*water_con)

            if args.solute is not None and not args.solute_inmem:
                for i,sol in enumerate(args.solute):
                    logger.info("Adding "+args.solute_con[i]+" "+args.solute[i]+" to the water volume")
                    grid_file, solute_vol      = pdbvol(args.solute[i])
                    created.append(grid_file)
                    if args.solute_con[i].endswith("M") and is_number(args.solute_con[i][:-1]):
                        solute_up       = int(float(args.solute_con[i][:-1])*((water_vol_up*avogadro/(1*10**27))))
                        solute_down     = int(float(args.solute_con[i][:-1])*((water_vol_down*avogadro/(1*10**27))))
                    elif args.solute_con[i].endswith("%") and is_number(args.solute_con[i][:-1]):
                        solute_up       = int(float(args.solute_con[i][:-1])/100*((water_vol_up*avogadro/(1*10**27))))
                        solute_down       = int(float(args.solute_con[i][:-1])/100*((water_vol_down*avogadro/(1*10**27))))
                    elif is_number(args.solute_con[i]):
                        try:
                            int(args.solute_con[i]) 
                        except:
                            logger.error("A number less than 1 is specified. If a concentration was intended, add M as a suffix!")
                            exit()
                        solute_up       = int(round(int(args.solute_con[i])*(water_vol_up/(water_vol_up+water_vol_down))))
                        solute_down     = int(round(int(args.solute_con[i])*(water_vol_down/(water_vol_up+water_vol_down))))
                    else:
                        logger.error("The format used to specify the number of molecules is not correct! It has to be a number or a number with an M/% suffix.")
                        exit()
                    solute_vol_up   = solute_vol*solute_up
                    solute_vol_down = solute_vol*solute_down
                    watnum_up       = watnum_up-int((solute_vol_up)*water_con)
                    watnum_down     = watnum_down-int((solute_vol_down)*water_con)
                    if solute_up+solute_down < 1:
                        logger.error("The solute concentration is too low for the calculated box size. Try increasing the concentration.")
                        exit()
                    charge_solute += args.solute_charge[i]*(solute_down+solute_up)
                    if not args.solvate:
                        if solute_down > 0:
                            content_solute += "structure "+args.solute[i]+"\n"
                            content_solute += "  nloop "+str(nloop)+"\n"
                            content_solute += "  number "+str(solute_down)+"\n"
                            content_solute += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(-leaflet_z+z_offset)+"\n"
                            content_solute += "end structure\n\n"
                        if solute_up > 0:
                            content_solute += "structure "+args.solute[i]+"\n"
                            content_solute += "  nloop "+str(nloop)+"\n"
                            content_solute += "  number "+str(solute_up)+"\n"
                            content_solute += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(leaflet_z+z_offset)+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                            content_solute += "end structure\n\n"
                    else:
                        content_solute += "structure "+args.solute[i]+"\n"
                        content_solute += "  nloop "+str(nloop)+"\n"
                        content_solute += "  number "+str(solute_down+solute_up)+"\n"
                        content_solute += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                        content_solute += "end structure\n\n"

            elif (args.solute is not None or args.solute_con is not None) and not args.solute_inmem:
                logger.error("The solute parameters are incomplete. Please include both a solute pdb and a concentration.")
                exit()
				
            ################################# SALT & CHARGES #############################
            
            pos_up = pos_down = 0
            neg_up = neg_down = 0
            charge = charges[bilayer]
            if not args.solvate:
                for leaflet in composition[bilayer]:
                    for lipid in composition[bilayer][leaflet]:
                        charge_lip += int(round(composition[bilayer][leaflet][lipid]*lipnum_dict[bilayer][leaflet]))*int(parameters[lipid]["charge"])
            if charge_lip != 0:
                logger.debug("The lipids contribute a charge of "+str(charge_lip)+" to the system. It will be considered for the neutralization.")
                charge += charge_lip
            if charge_solute != 0:
                logger.debug("The solutes contribute a charge of "+str(charge_solute)+" to the system. It will be considered for the neutralization.")
                charge += charge_solute
            if charge > 0:
                neg_up = int(round(charge*(water_vol_up/(water_vol_up+water_vol_down))))
                neg_down = int(round(charge*(water_vol_down/(water_vol_up+water_vol_down))))
            else:
                pos_up = int(round((abs(charge)/ion_dict[args.salt_c][1])*(water_vol_up/(water_vol_up+water_vol_down))))
                pos_down = int(round((abs(charge)/ion_dict[args.salt_c][1])*(water_vol_down/(water_vol_up+water_vol_down))))
            con_pos = (pos_up+pos_down)/((water_vol_up+water_vol_down)*avogadro/(1*10**27))
            con_neg = (neg_up+neg_down)/((water_vol_up+water_vol_down)*avogadro/(1*10**27))
            if args.salt:
                if con_pos > saltcon or con_neg > saltcon*ion_dict[args.salt_c][1]:
            #        print pos_up, pos_down, neg_up, neg_down, saltnum_up, saltnum_down
                    logger.warning("""
            The concentration of ions required to neutralize the system is higher than the concentration specified. 
            Either increase the salt concentration by using the --saltcon flag or run the script without the --salt flag.""")
                    logger.info("Positive ion concentration: "+str(round(con_pos,3)))
                    logger.info("Negative ion concentration: "+str(round(con_neg,3)))
                    logger.info("Salt concentration specified: "+str(saltcon))
                    if override_salt:
                        if args.verbose:
                            logger.info("Overriding salt concentration...")
                        saltcon = max(con_pos,con_neg)
                        pass
                    else:
                        exit()
                saltnum_up   = int(water_vol_up*saltcon*avogadro/(1*10**27))
                saltnum_down = int(water_vol_down*saltcon*avogadro/(1*10**27))
            
                if abs(charge)/2 < min(saltnum_up,saltnum_down):
                    pos_up   += saltnum_up-abs(charge)/(ion_dict[args.salt_c][1]*2)
                    pos_down += saltnum_down-abs(charge)/(ion_dict[args.salt_c][1]*2)
                    neg_up   += saltnum_up*ion_dict[args.salt_c][1]-abs(charge)/2
                    neg_down += saltnum_down*ion_dict[args.salt_c][1]-abs(charge)/2
    
                if args.charge_imbalance != 0:
                    if n % 2 == 0:
                        if args.imbalance_ion == "cat":
                            pos_up   += args.charge_imbalance/ion_dict[args.salt_c][1]
                            pos_down -= args.charge_imbalance/ion_dict[args.salt_c][1]
                        else:
                            neg_up   -= args.charge_imbalance
                            neg_down += args.charge_imbalance
                    else:
                        if args.imbalance_ion == "cat":
                            pos_up   -= args.charge_imbalance/ion_dict[args.salt_c][1]
                            pos_down += args.charge_imbalance/ion_dict[args.salt_c][1]
                        else:
                            neg_up   += args.charge_imbalance
                            neg_down -= args.charge_imbalance
            
                charge_data= """
            Salt and charge info:
            Upper positive charges   = %-9s 
            Lower positive charges   = %-9s    
            Upper negative charges   = %-9s     
            Lower negative charges   = %-9s    
            Charge imbalance         = %-9s
            Upper salt number        = %-9s    
            Lower salt number        = %-9s    
            """
                logger.debug(charge_data % ( pos_up, pos_down, neg_up, neg_down, args.charge_imbalance, saltnum_up, saltnum_down ))
                
                checkup = [pos_up,pos_down,neg_up,neg_down]
                if any(v < 0 for v in checkup):
                    logger.critical("The applied charge imbalance caused a negative number of ions! Check your input")
                    exit()
            
            
            #    new_con_neg = (neg_up+neg_down)/(((water_vol_up+water_vol_down)*avogadro/(1*10**27)))
            #    new_con_pos = (pos_up+pos_down)/(((water_vol_up+water_vol_down)*avogadro/(1*10**27)))

            try:
                shutil.copy(os.path.join(script_path, rep, "pdbs", cation+".pdb"), "./")
            except:
                pdbtar.extract(cation+".pdb")
            created_notrun.append(cation+".pdb")
            try:
                shutil.copy(script_path+rep+"/pdbs/"+"CLA.pdb", "./")
            except:
                pdbtar.extract("CLA.pdb")
            created_notrun.append("CLA.pdb")
            if not args.solvate:
                if pos_down > 0:
                    content_ion += "structure "+cation+".pdb\n"
                    content_ion += "  nloop "+str(nloop)+"\n"
                    content_ion += "  number "+str(int(pos_down))+"\n"
                    content_ion += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(-leaflet_z+z_offset)+"\n"
                    content_ion += "end structure\n\n"
                if pos_up > 0:
                    content_ion += "structure "+cation+".pdb\n"
                    content_ion += "  nloop "+str(nloop)+"\n"
                    content_ion += "  number "+str(int(pos_up))+"\n"
                    content_ion += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(leaflet_z+z_offset)+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                    content_ion += "end structure\n\n"
                if neg_down > 0:
                    content_ion += "structure CLA.pdb\n"
                    content_ion += "  nloop "+str(nloop)+"\n"
                    content_ion += "  number "+str(int(neg_down))+"\n"
                    content_ion += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(-leaflet_z+z_offset)+"\n"
                    content_ion += "end structure\n\n"
                if neg_up > 0:
                    content_ion += "structure CLA.pdb\n"
                    content_ion += "  nloop "+str(nloop)+"\n"
                    content_ion += "  number "+str(int(neg_up))+"\n"
                    content_ion += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(leaflet_z+z_offset)+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                    content_ion += "end structure\n\n"
            else:
                if pos_down+pos_up > 0:
                    content_ion += "structure "+cation+".pdb\n"
                    content_ion += "  nloop "+str(nloop)+"\n"
                    content_ion += "  number "+str(int(pos_down+pos_up))+"\n"
                    content_ion += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                    content_ion += "end structure\n\n"
                if neg_down+neg_up > 0:
                    content_ion += "structure CLA.pdb\n"
                    content_ion += "  nloop "+str(nloop)+"\n"
                    content_ion += "  number "+str(int(neg_down+neg_up))+"\n"
                    content_ion += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                    content_ion += "end structure\n\n"

            if not args.solvate:
                content_water += "structure TIP3P.pdb\n"
                content_water += "  nloop "+str(nloop)+"\n"
                content_water += "  number "+str(watnum_down)+"\n" # deleted -pos_down-neg_down. Was it necessary?
                content_water += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(-leaflet_z+z_offset)+"\n"
                content_water += "end structure\n\n"
                content_water += "structure TIP3P.pdb\n"
                content_water += "  nloop "+str(nloop)+"\n"
                content_water += "  number "+str(watnum_up)+"\n" # deleted -pos_up-neg_up. Was it necessary?
                content_water += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(leaflet_z+z_offset)+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                content_water += "end structure\n\n"
            else:
                content_water += "structure TIP3P.pdb\n"
                content_water += "  nloop "+str(nloop)+"\n"
                content_water += "  number "+str(watnum_down+watnum_up)+"\n" # deleted -pos-neg. Was it necessary?
                content_water += "  inside box "+str(round(X_min,2))+" "+str(round(Y_min,2))+" "+str(round(Z_dim[bilayer][0]+z_offset,2))+" "+str(round(X_max,2))+" "+str(round(Y_max,2))+" "+str(round(Z_dim[bilayer][1]+z_offset,2))+"\n"
                content_water += "end structure\n\n"					

        contents = content_header+content_prot+content_lipid+content_water+content_ion+content_solute

        with open(args.packlog+".inp","w+") as script:
    
    #        script = open(args.packlog+".inp","w+")
            script.write(contents)
    #        script = script.seek(0, os.SEEK_SET) # reset script file pointer for packmol
            script.seek(0, os.SEEK_SET)

            if not args.solvate:
                box_data = """
                Lower water box vol    = %-9s A^3
                Upper water box vol    = %-9s A^3
                Mem. upper leaflet vol = %-9s A^3
                Mem. lower leaflet vol = %-9s A^3
                
                """
                
                logger.debug(box_data % (round(water_vol_down,2), round(water_vol_up,2), round(lipid_vol_up,2), round(lipid_vol_down,2)))
            else:
                box_data = """
                Water box vol    = %-9s A^3
                
                """
    
                logger.debug(box_data % (round(water_vol_down+water_vol_up,2)))
    
            logger.debug("Script for packmol written to "+args.packlog+".inp")
    
            if packmol != None and os.path.exists(packmol) and run:
                logger.info("\nRunning Packmol...")
                log = open(args.packlog+".log","w")
                p = subprocess.Popen(packmol, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=script)#, env=my_env)
#                p = subprocess.Popen(packmol, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=content_header+content_prot+content_lipid+content_water+content_ion+content_solute)
                global pid
                pid = p.pid
                def kill_child():
                    if pid is None:
                        pass
                    else:
                        try:
                            os.kill(pid, signal.SIGTERM)
                        except:
                            pass
                atexit.register(kill_child)
                GENCAN_track = 0
                TOTGENCAN    = 0
                FRAME        = 0
                energy_values = []
                starting = True
                all_together = False
                progress_bar = True
                step = 1
                while True:
                    output = p.stdout.readline().decode('utf-8')
                    log.write(output)
                    if output == '' and p.poll() is not None:
                        break
                    if "Number of independent structures:" in output.strip():
                        structs = output.strip().split()[-1]
                        if not args.packall:
                            if not onlymembrane:
                                TOTAL = int(nloop)*(int(structs)-1)+nloop_all
                            else:
                                TOTAL = int(nloop)*(int(structs))+nloop_all
                        else:
                            TOTAL = int(nloop_all)
                        mag   = int(math.log10(TOTAL))+2
                        if not onlydots:
                            p_bar = tqdm.tqdm(total=TOTAL)
                            
                    if "Starting GENCAN loop"  in output.strip():
                        if GENCAN_track < nloop+1:
                            GENCAN_track += 1; TOTGENCAN += 1
                        if onlydots:
                            if GENCAN_track % 10 == 0:
                                sys.stdout.write(".")
                                sys.stdout.flush()
                        if not onlydots and GENCAN_track <= nloop:
                            p_bar.update(1)
                    if "Packing molecules " in output.strip():
                        if onlydots:
                            sys.stdout.write("\033[F")
                            sys.stdout.write("\r" +"\nProcessing segment "+output.strip().split()[-1]+" of "+structs)
                            sys.stdout.flush()
                        else:
                            if int(nloop)-GENCAN_track > 0 and not starting:
                                p_bar.update(int(nloop)-GENCAN_track)
                            else:
                                starting = False
                            p_bar.set_description("Molecule segment %s/%s" % (output.strip().split()[-1],structs))
                        GENCAN_track = 0
                    if ("Current solution written to file" in output.strip() or "Writing current (perhaps bad) structure to file" in output.strip()) and args.traj:
                        FRAME += 1
                        shutil.move(outfile,outfile.replace(".pdb","_"+("{:0"+str(mag)+"d}").format(FRAME)+".pdb"))
                    if "Packing all molecules together" in output.strip():
                        all_together = True
                        if not onlydots:
                            if not args.packall:
                                p_bar.update(int(nloop)-GENCAN_track)
                            p_bar.set_description("All-together Packing")
                        logger.debug("\nIndividual packing processes complete. Initiating all-together packing. This might take a while!")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\r" +"\nAll-together Packing")
                        sys.stdout.flush()
                        GENCAN_track = 0
                        nloop = nloop_all
                    if "All-type function" in output.strip():
                        fnx_value = float(output.strip().split()[-1])
                        energy_values.append((TOTGENCAN,fnx_value))
                    if "Function value from last GENCAN" in output.strip() and all_together:
                        fnx_value = float(output.strip().split()[-1])
                        energy_values.append((TOTGENCAN,fnx_value))
                    if "Success!" in output.strip():
                        p_bar.update(int(nloop)-GENCAN_track)        
        
#            script.close()
    
        if run and args.plot:
            try:
                import matplotlib
                matplotlib.use('Agg')
                import matplotlib.pyplot as plt
                matplotlib.rcParams["font.sans-serif"]='Arial'
                plt.plot(*list(zip(*energy_values)),color="black")
                plt.xlabel('Iteration')
                plt.ylabel('Objective function')
                plt.savefig(outfile+'.png')
            except:
                logger.error("Matplotlib could not be imported. Check that you have a working version.")
            with open("GENCAN.log","w") as gencanlog:
                gencanlog.write("\n".join("%s %s" % x for x in energy_values))
    
        elif not os.path.exists(packmol) or not run:
            logger.info("The script generated can be run by using a packmol executable ($PACKMOL_PATH < memgen.inp)")
            exit()
        
        if not args.charmm:
            logger.info("Transforming to AMBER")
            charmmlipid2amber(outfile,outfile, os.path.join(script_path, lib, "charmmlipid2amber", "charmmlipid2amber.csv"))
       
    
    if not args.charmm: 
        if args.parametrize:
            logger.info("Parametrizing using LEaP")
            if args.solvate:
                infiles = prepare_leap(outfile,ligands=args.ligand_param,cation=args.salt_c,suffix="_wat",custom_lines=args.leapline,gaff2=args.gaff2)
            else:
                infiles = prepare_leap(outfile,ligands=args.ligand_param,cation=args.salt_c,suffix="_lipid",custom_lines=args.leapline,gaff2=args.gaff2,extended=extended)
            if os.stat(infiles[1]).st_size == 0:
                args.minimize = False
                logger.error("Parametrization Failed! Check that all the components parameters are included (leap_.log file for details)")
                exit()
            created.extend(("leap.log","leap_ .log","leap.in"))
        if args.minimize:
            print("Minimizing...")
            if args.pdb is not None:
                prot_residues = sum([len(pdb_parse(pdb)) for pdb in args.pdb])
            if args.sander:
                engine = "sander"
            else:
                engine = "pmemd"
            if args.pdb is not None:
                minimized_restraint = amber_minimize(infiles,"(:1-"+str(prot_residues)+" | @P31)", sd_steps = 250, cg_steps = 250,engine=engine)
            else:
                minimized_restraint = amber_minimize(infiles,sd_steps = 250, cg_steps = 250,engine=engine)
            if os.stat(minimized_restraint[0]).st_size == 0:
                args.minimize = False
                logger.error("Restrained minimization Failed! (log files for details)")
                exit()
            minimized = amber_minimize(minimized_restraint, sd_steps = 250, cg_steps = 250,min_script=outfile.replace(".pdb","_min.in"),engine=engine,outfile=outfile.replace(".pdb","_min.pdb"))
            if os.stat(minimized[0]).st_size == 0:
                args.minimize = False
                logger.error("Minimization Failed! (log files for details)")
                exit()
    
    if delete:
        print("Deleting temporary files...")
        for file in created:
            try:
                os.remove(file)
            except:
                pass
        if run:
            for file in created_notrun:
                try:
                    os.remove(file)
                except:
                    pass
    
    print("DONE!")

if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    
    streamer = logging.StreamHandler()
    streamer.setLevel(logging.INFO)
    logger.addHandler(streamer)
    
    logger.info(head)
    
    args = parser.parse_args()
    main(args)
