#!/bin/bash

# Print the input file for methane
cat > methane.in << EOF
HF ncyc=3 BASIS=3-21g denserms=1.0e-5 opt

C        0.000000000    0.000000000    0.000000000
H        0.000000000    0.000000000    1.094000000
H        1.031440860    0.000000000   -0.364644693
H       -0.515720430    0.893253987   -0.364644693
H       -0.515720430   -0.893253987   -0.364644693
EOF

# This will put the output in methane.out, no redirect needed
${AMBERHOME}/bin/quick methane.in

# Take the important details from the file
grep "[1-5][X-Z]" methane.out > result.txt
grep "ELECTRONIC ENERGY    =" methane.out | tail -1 >> result.txt
grep "CORE_CORE REPULSION  =" methane.out | tail -1 >> result.txt
grep "TOTAL ENERGY         =" methane.out | tail -1 >> result.txt

# Take the important details from the file
${AMBERHOME}/AmberTools/test/dacdif -r 1.0e-4 result.txt.save result.txt

# Check for a CUDA-enabled quick, then run this if such a thing is
# found.  If the machine does not have a GPU available, skip the rest.
if [ -e ${AMBERHOME}/bin/quick.cuda ] ; then
  ${AMBERHOME}/bin/quick.cuda methane.in > cuda_catch.txt
  N=0
  P=0
  for GG in `tail -1 cuda_catch.txt` ; do
    if [ ${N} -eq 0 ] && [ ${GG} == "NO" ] ; then
      let "P+=1"
    fi
    if [ ${N} -eq 1 ] && [ ${GG} == "CUDA-Enabled" ] ; then
      let "P+=1"
    fi
    let "N+=1"
  done
  if [ ${P} -lt 2 ] ; then
    grep "[1-5][X-Z]" methane.out > result_cuda.txt
    grep "ELECTRONIC ENERGY    =" methane.out | tail -1 >> result_cuda.txt
    grep "CORE_CORE REPULSION  =" methane.out | tail -1 >> result_cuda.txt
    grep "TOTAL ENERGY         =" methane.out | tail -1 >> result_cuda.txt
    ${AMBERHOME}/AmberTools/test/dacdif \
      result_cuda.txt.save result_cuda.txt \
      -r 1.0e-4
  else
    echo "No cuda-enabled device was detected."
    echo "WARNING: a cuda-enabled quick binary was found but not tested."
    echo "=============================================================="
  fi
fi

# Clean up
rm methane.in methane.out methane.dat
