#!/bin/bash

# Print the input file for methane
cat > benzene.in << EOF
HF BASIS=3-21G CUTOFF=1.0d-10 ENERGY DENSERMS=1.0d-8

 C     0.000000     0.000000     0.000000
 C     0.000000     0.000000     1.450000
 C     1.255395     0.000000    -0.407902
 C     2.107683     0.000000     0.765172
 C     1.346976    -0.700296     1.781740
 C     1.346976     0.700296     1.781740
 H     3.151682     0.000000     0.455355
 H    -1.026719     0.000000     1.813000
 H     1.453627    -1.774649     1.639216
 H     1.453627     1.774649     1.639216
 H     1.283354     0.000000    -1.496543
 H    -0.662507     0.000000    -0.864295
EOF

# This will put the output in methane.out, no redirect needed
${AMBERHOME}/bin/quick benzene.in

# Take the important details from the file
grep "[1-5][X-Z]" benzene.out > result.txt
grep "ELECTRONIC ENERGY    =" benzene.out | tail -1 >> result.txt
grep "CORE_CORE REPULSION  =" benzene.out | tail -1 >> result.txt
grep "TOTAL ENERGY         =" benzene.out | tail -1 >> result.txt

# Take the important details from the file
${AMBERHOME}/AmberTools/test/dacdif -r 1.0e-4 result.txt.save result.txt

# Check for a CUDA-enabled quick, then run this if such a thing is
# found.  If the machine does not have a GPU available, skip the rest.
if [ -e ${AMBERHOME}/bin/quick.cuda ] ; then
  ${AMBERHOME}/bin/quick.cuda benzene.in > cuda_catch.txt
  N=0
  P=0
  for GG in `tail -1 cuda_catch.txt` ; do
    if [ ${N} -eq 0 ] && [ ${GG} == "NO" ] ; then
      let "P+=1"
    fi
    if [ ${N} -eq 1 ] && [ ${GG} == "CUDA-Enabled" ] ; then
      let "P+=1"
    fi
    let "N+=1"
  done
  if [ ${P} -lt 2 ] ; then
    grep "[1-5][X-Z]" benzene.out > result_cuda.txt
    grep "ELECTRONIC ENERGY    =" benzene.out | tail -1 >> result_cuda.txt
    grep "CORE_CORE REPULSION  =" benzene.out | tail -1 >> result_cuda.txt
    grep "TOTAL ENERGY         =" benzene.out | tail -1 >> result_cuda.txt
    ${AMBERHOME}/AmberTools/test/dacdif \
      result_cuda.txt.save result_cuda.txt \
      -r 1.0e-4
  else
    echo "No cuda-enabled device was detected."
    echo "WARNING: a cuda-enabled quick binary was found but not tested."
    echo "=============================================================="
  fi
fi

# Clean up
rm benzene.in benzene.out benzene.dat
