This directory tree contains AmberTools19 and (optionally) Amber18.  All
contents are protected by Copyright and Licensing provisions.

If you don't see a "configure" script in this directory, you probably have
Amber18 but not AmberTools19.  Go to http://ambermd.org, and click on the
"Download Amber" link.  Un-tar the AmberTools19.tar.bz2 file into this
directory tree.

------------------------------------------------------------------------------

All of the documentation is in the "doc" subdirectory, in the *.pdf
files.  See Chapter 2 in doc/Amber19.pdf for complete installation
instructions.  For the impatient, installation starts by typing
"./configure --help" in this directory.

For more information, including tutorials, bug fixes, etc., please consult
the Amber Web page:

               http://ambermd.org

------------------------------------------------------------------------------

Except as noted below, Amber 18 is provided under a license that has
restrictions on its use and redistribution.  You should not use Amber 18
unless you have executed a license agreement with UCSF.  Consult that
license to see the provisions that apply.

The files in the "AmberTools" subdirectory are covered by a separate, open
source, license; see ./AmberTools/LICENSE for more information.

The files in the "dat/leap" subdirectory are in the public domain.

------------------------------------------------------------------------------
