
#fix the unit tests, which find ndiff and dacdif relative to AMBERHOME
install(PROGRAMS ndiff.awk dacdif numprocs.awk DESTINATION "${CMAKE_INSTALL_POSTFIX}test")

if(INSTALL_TESTS)
	install(DIRECTORY . USE_SOURCE_PERMISSIONS DESTINATION "${CMAKE_INSTALL_POSTFIX}test/" COMPONENT Tests)
endif()